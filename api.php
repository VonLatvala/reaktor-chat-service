<?php
    /*
     * File:
     *  api.php
     * Summary:
     *  Handles the serverside
     * 
     * Modified 27.06.2014 16:54 GMT+2, Axel Latvala
     */
    
    ini_set('display_errors', 1);
    error_reporting(E_ALL | E_STRICT);
    
    header('Content-type: application/json; charset=utf-8');
    
    require_once 'inc/env_vars.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'APIResponse.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'system.class.php';
    
    function ajax_error_handler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno)) {
            // This error code is not included in error_reporting
            return;
        }

        $apiResponse = new APIResponse();
        $apiResponse->errorFile = $errfile;
        $apiResponse->errorLine = $errline;

        switch ($errno) {
        case E_USER_ERROR:
        case E_ERROR:
            $apiResponse->setError('PHP FATAL ERROR ('.$errno.'): '.$errstr.' at '.$errfile.':'.$errline,
                    APIResponse::$i18n[LOCALE]['E_UNEXPECTED'],
                    $errno);
            die($apiResponse);
            break;

        case E_USER_WARNING:
        case E_WARNING:
            $apiResponse->setWarn('PHP WARNING ('.$errno.'): '.$errstr.' at '.$errfile.':'.$errline,
                    APIResponse::$i18n[LOCALE]['W_UNEXPECTED'],
                    $errno);
            die($apiResponse);
            break;

        case E_USER_NOTICE:
        case E_NOTICE:
            $apiResponse->setNotice('PHP NOTICE ('.$errno.'): '.$errstr.' at '.$errfile.':'.$errline,
                    APIResponse::$i18n[LOCALE]['N_UNEXPECTED'],
                    $errno);
            die($apiResponse);
            break;

        default:
            $apiResponse->setError('PHP UNEXPECTED ERROR ('.$errno.'): '.$errstr.' at '.$errfile.':'.$errline,
                    APIResponse::$i18n[LOCALE]['E_UNEXPECTED'],
                    $errno);
            die($apiResponse);
            break;
        }

        /* Don't execute PHP internal error handler */
        return true;
    }
    
    set_error_handler('ajax_error_handler');
    
    $system = new System();
    
    $system->run_api();