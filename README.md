# Reaktor Chat Service #

Hello! This is a chat service built to demonstrate my skills. It's not perfect, but it gets the job done. I have not used any framework for the backend, I've built that one myself. The backend uses css-to-inlin-styles library to make pretty emails. The frontend uses bootstrap, jQuery, moment and responsive-toolkit. The app runtime is built by myself, no frameworks used.

### Capabilities ###

* User management (Register, Change password, Toggle Admin/Debug/Banned)
* Cross-Application user information and password storage, this is mainly because I developed this "framework" to be used in in-house web applications, where users couldn't be bothered to have different usernames and passwords for different apps. Users have DIFFERENT permission to different apps, and the apps have to be notified on a per-app basis to permit a user to login. The authorized_users table is supposed to exist in every app that uses this scheme.
* Optional registration token protection for registering new users, if this is enabled, an admin has to have the system send a mail to the new registrant which will include the registration token [default disabled]
* Optional grecaptcha protection for registering [default enabled]
* Searching for users and public groupchats
* Creating groupchats that are public/hidden
* Chatting with users and groupchats in realtime

### How do I get set up? ###

* Setting up is very easy, I have configured vagrant to automatically pull all relevant repos and start a local server. If you can't be bothered to use vagrant, I host a live version at https://reaktor-chat-service.alatvala.fi
* In case you decide that you do want to use vagrant, follow the steps listed in this apps vagrant repository: https://bitbucket.org/VonLatvala/reaktor-chat-service-vagrant
* In case you don't want to use vagrant, but still want to run this on your own server, be sure to use the scripts in https://bitbucket.org/VonLatvala/reaktor-chat-service-sql to prep your database.

### How do I configure optional features ###

The application has a file called `config.php` which resides in `/inc/config.php`. This file has all sorts of options you can toggle. The vagrant version automatically patches the `env_vars.php` file, which resides in `/inc/env_vars.php`. Usually you only would need to edit `APACHE_PATH` and/or `PROJECT_PATH`. `APACHE_PATH` is where your `passwords.php` file should live. That directory should NOT be public. This file includes secret information used by the app to speak with the database, authorize users with google recaptcha and so on. Here's an example of the `passwords.php` file:


    <?php
        define('APP_DATABASE_PASSWORD', '********');
        define('LOGIN_DATABASE_PASSWORD', '********');
    
        define('RECAPTCHA_SITE_KEY', '********');
        define('RECAPTCHA_SECRET_KEY', '********');
    
        define('GA_TRACKING_ID', '********');

## How do I use the UI? ##

You can change your password from User Details up in the navbar.

Up in the navigation bar are for logged out users two links, one to the login form and another for the registration form. If you are using the Vagrant version, you can login with the administratior user with these credentials:
User `admin`
Password `vengenfulSpirit`
The administrator has an own area he can manage users in, this one is found from the navbar on the top of the page.

For common logged in users (and administrators!), there is the Dashboard, also found from the navbar. Here users can search for other users, search for chats, join chats, leave chats, and send messages to chats and other users.

To create a groupchat, enter the preferred name into the search bar above the chat list. Make sure to type a minimum of 3 non-whitespace characters. The chatlist will be filled with chats matching what you typed to the searchbar, and after all of them, there will be an element with two buttons which will let you create either a public or a hidden groupchat. Hidden groupchats are not displayed when people search (or list ALL with %%%) for chats and users. Please make sure to first type in the whole name of your chat before you choose which kind of chat you want to make, because what you have entered to the chat search WILL be your new groupchat's name.

In case you created a hidden groupchat, you would have to look to the right where you have your chat's name, and to the right of it is a button with three dots. Press this button to show the chat details, which will tell you what UUID your hidden groupchat has. You can share this UUID to other people and ask them to enter it to the chat search box, and if entered correctly, the chat will be visible to them. The users can then click the chat to join it.

From this same chat details view, you can leave chats.

© Axel Latvala