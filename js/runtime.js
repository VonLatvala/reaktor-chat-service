/* global moment, grecaptcha, ResponsiveBootstrapToolkit */

'use strict';
var System = function()
{
    var that = this;
    var $loadingView = $('.view#loadingView'),
        $loginView = $('.view#loginView'),
        $registerView = $('.view#registerView'),
        $dashboardView = $('.view#dashboardView'),
        $userProfileView = $('.view#userProfileView'),
        $adminCtlView = $('.view#adminCtlView'),
        $curView = $loadingView;
    
    var $inputLoginUser = $('#inputLoginUser'),
        $inputLoginPassword = $('#inputLoginPassword');
    
    var $inputPasschangeOldPass = $('#inputPasschangeOldPass'),
        $inputPasschangeNewPass = $('#inputPasschangeNewPass'),
        $inputPasschangeNewPassConfirm = $('#inputPasschangeNewPassConfirm');
    
    var $loginButton = $loginView.find('button#loginButton'),
        $showChatList = $('#showChatlist');
    
    var curUser = null;
    var adminPanelUserList = [];
    var chats = {};
    var serverTimeOffset = 0;
    
    var init = function()
    {
        $.datepicker.setDefaults({
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''});
        
        that.resetFields();
        bindFunctions();
        loginStatusInit();
    };
    
    that.resetFields = function()
    {
        that.clearChatListUI();
        that.clearChatMessageRows();
    };
    that.setView = function($newView)
    {
        $curView.finish();
        if($curView.is($newView))
            return;
        var fadeTime = 300;
        $('.navbar-nav li').removeClass('active');
        switch ($newView)
        {
            case $dashboardView:
                $('#navDashboard').parent().addClass('active');
                break;
            case $loginView:
                $('#navLoginBtn').parent().addClass('active');
                break;
            case $registerView:
                $('#navRegisterBtn').parent().addClass('active');
                break;
            case $userProfileView:
                $('#navUserButton').parent().addClass('active');
                break;
            case $adminCtlView:
                $('#navAdminButton').parent().addClass('active');
                break;
        }
        $curView.fadeOut(fadeTime, function()
        {
            $curView = $newView;
            $curView.fadeIn(fadeTime);
            that.resizeChatHistory();
            that.scrollDownChatHistory();
        });
    };
    var loginStatusInit = function()
    {
        checkLoginStatus(function(data)
        {
            if(data !== false && typeof data === 'object' && data.loggedIn === true)
            {
                curUser = data.user;
                onLoggedIn();
            }
            else
            {
                curUser = null;
                exitLoading();
            }
        });
    };
    var refreshNav = function()
    {
        if(curUser !== null)
        {
            if(curUser.is_admin)
                $('.showOnJSActivation').show();
            $('#navbar ul.nav').addClass('nav-loggedin');
            $('#navbar ul.nav').removeClass('nav-loggedout');
        }
        else
        {
            $('.showOnJSActivation').hide();
            $('#navbar ul.nav').removeClass('nav-loggedin');
            $('#navbar ul.nav').addClass('nav-loggedout');
        }
    };
    var loadAdminPanelUsers = function(hSucc)
    {
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
        },
        dataObj = 
        {
            q:'listUsers'
        },
        apiFail = function()
        {
            console.error('API error');
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        reqFail = function(data)
        {
            console.error(data.systemMsg);
            exit();
        },
        reqSuccess = function(data)
        {
            adminPanelUserList = data.data;
            if(typeof hSucc === 'function') hSucc();
            exit();
        },
        exit = function(response)
        {
            
        };
        init();
    };
    
    that.userPropEditor = function(uuid, prop, bool, hSucc, hErr, always)
    {
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
        },
        dataObj = 
        {
            q:'userPropEditor',
            uuid: uuid,
            prop: prop,
            bool: bool
        },
        apiFail = function()
        {
            console.error('API error');
            exit(false);
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        reqFail = function(data)
        {
            console.error(data.systemMsg);
            exit(false);
        },
        reqSuccess = function(data)
        {
            exit(true);
        },
        exit = function(response)
        {
            if(response) {
                if(typeof hSucc === 'function')
                    hSucc();
            }
            else {
                if(typeof hErr === 'function')
                    hErr();
            }
            if(typeof always === 'function')
                always();
        };
        init();
    };
    
    var renderAdminPanelUsers = function(hSucc)
    {
        var $userManagerPanelBody = $('#userManagerPanelBody');
        $userManagerPanelBody.empty();
        var $table = $('<table></table>').addClass('table').addClass('table-striped').addClass('table-hover');
        var $banCol = $('<th></th>').text('Ban');
        var $adminCol = $('<th></th>').text('Admin');
        var $uuidCol = $('<th></th>').text('UUID');
        var $usernameCol = $('<th></th>').text('Username');
        var $nameCol = $('<th></th>').text('Name');
        var $mailCol = $('<th></th>').text('Email');
        var $row = $('<tr></tr>').append($uuidCol).append($usernameCol).append($nameCol).append($mailCol).append($banCol).append($adminCol);
        var $thead = $('<thead></thead>').append($row);
        $table.append($thead);
        var $tbody = $('<tbody></tbody>').appendTo($table);
        $table.append($tbody);
        $userManagerPanelBody.append($table);
        $.each(adminPanelUserList, function(key, user){
            var $banBtn,
                $unbanBtn,
                $adminBtn,
                $deadminBtn;
            $banBtn = $('<button></button>').text('Ban').addClass('btn').addClass('btn-danger').click((function(user){return function(){
                    $banBtn.prop('disabled', true);
                    that.userPropEditor(user.user_uuid, 'is_banned', true, function(){$banBtn.hide();$unbanBtn.show();}, function(){}, function(){$banBtn.prop('disabled', false);});
                };})(user));
            $unbanBtn = $('<button></button>').text('Unban').addClass('btn').addClass('btn-success').click((function(user){return function(){
                    $unbanBtn.prop('disabled', true);
                    that.userPropEditor(user.user_uuid, 'is_banned', false, function(){$unbanBtn.hide();$banBtn.show();}, function(){}, function(){$unbanBtn.prop('disabled', false);});
                };})(user));
            $adminBtn = $('<button></button>').text('Admin').addClass('btn').addClass('btn-success').click((function(user){return function(){
                    $adminBtn.prop('disabled', true);
                    that.userPropEditor(user.user_uuid, 'is_admin', true, function(){$adminBtn.hide();$deadminBtn.show();}, function(){}, function(){$adminBtn.prop('disabled', false);});
                };})(user));
            $deadminBtn = $('<button></button>').text('Deadmin').addClass('btn').addClass('btn-danger').click((function(user){return function(){
                    $deadminBtn.prop('disabled', true);
                    that.userPropEditor(user.user_uuid, 'is_admin', false, function(){$deadminBtn.hide();$adminBtn.show();}, function(){}, function(){$deadminBtn.prop('disabled', false);});
                };})(user));
            var $banCol = $('<td></td>').append($banBtn).append($unbanBtn);
            var $adminCol = $('<td></td>').append($adminBtn).append($deadminBtn);
            var $uuidCol = $('<td></td>').text(user.user_uuid);
            var $usernameCol = $('<td></td>').text(user.username);
            var $nameCol = $('<td></td>').text(user.first_name+' '+user.last_name);
            var $mailCol = $('<td></td>').text(user.email);
            if(user.is_banned)
                $banBtn.hide();
            else
                $unbanBtn.hide();
            if(user.is_admin)
                $adminBtn.hide();
            else
                $deadminBtn.hide();
            var $row = $('<tr></tr>').append($uuidCol).append($usernameCol).append($nameCol).append($mailCol).append($banCol).append($adminCol);
            $tbody.append($row);
        });
    };
    var onLoggedIn = function()
    {
        enterLoading();
        that.loadingStage = 1;
        refreshNav();
        refreshUserProfile();
        renderNameOnDashboard();
        if(curUser.is_admin)
            loadAdminPanelUsers(renderAdminPanelUsers);
        var completedCallback = function(success)
        {
            if(success && ++that.loadingStage === 2)
            {
                that.loadingStage = null;
                exitLoading();
            }
        };
        completedCallback(true);
        that.getInit(false);
        /**
         * initialization after login, required loadingStage should equal initfunctions+1. Pass completedCallback to every init function.
         */
    };
    that.onLoggedOut = function()
    {
        that.setView($loginView);
        that.clearChatListUI();
        that.clearChatMessageRows();
        chats = [];
        refreshNav();
    };
    var exitLoading = function()
    {
        (function($, viewport){

            if(viewport.is('xs')) {
                $('#chatListCol').show();
                $('#chatMessageHistoryCol').hide();
            }
        })(jQuery, ResponsiveBootstrapToolkit);
        if(curUser !== null)
            that.setView($dashboardView);
        else if(typeof that.requestRegView !== 'undefined' && that.requestRegView === true)
            that.setView($registerView);
        else
            that.setView($loginView);
    };
    var enterLoading = function()
    {
        that.setView($loadingView);
    };
    var checkLoginStatus = function(statusCallback)
    {
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
        },
        dataObj = 
        {
            q:'loginStatus'
        },
        apiFail = function()
        {
            console.error('API error');
            exit(false);
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        reqFail = function(data)
        {
            console.error(data.systemMsg);
            exit(false);
        },
        reqSuccess = function(data)
        {
            exit(data.data);
        },
        exit = function(response)
        {
            statusCallback(response);
        };
        init();
    };
    var bindFunctions = function()
    {
        $loginButton.click(function(e){e.preventDefault(); onUILogin(e);});
        $loginView.find('input').keyup(function(e){if(e.which===13){e.preventDefault(); onUILogin(e);}});
        $('.navbar-nav a').click(hNavClick);
        $('#passchangeButton').click(function(e){e.preventDefault(); onUIPassChange(e);});
        $('#inputRegisterBirthdate').datepicker({showButtonPanel: true});
        $('#registerButton').click(function(e){e.preventDefault(); onUIRegister(e);});
        $('#userInviteButton').click(function(e){e.preventDefault(); onUIInviteNewUser(e);});
        $(window).resize(that.resizeChatHistory);
        that.resizeChatHistory();
        $('#sendNewChatMessageBtn').click(function(e){e.preventDefault(); that.onUISendMessage(e);});
        $('#inputChatMessage').keyup(function(e){if(e.which===13){e.preventDefault(); that.onUISendMessage(e);}});
        $('#chatSearchBox').keyup(that.onSearchBoxKeyup);
        $('#chatSearchBox').keydown(onSearchBoxKeydown);
        $('#clearChatSearchBtn').click(function(e){e.preventDefault(); $('#chatSearchBox').val(''); onSearchBoxKeydown(); that.resetChatListUI();});
        $showChatList.click(function(e){
            $('#chatMessageHistoryCol').hide();
            $('#chatListCol').show();
            $showChatList.hide();
        });
        (function($, viewport){
            var hOnWinResize = viewport.changed(
                function() {
                    if(viewport.is('xs')) {
                        $('#chatMessageHistoryCol').show();
                        $('#chatListCol').hide();
                        $showChatList.show();
                        $('#dashboardContainer').removeClass('nonMobilePadder');
                        that.resizeChatHistory();
                    }
                    if(viewport.is('>=sm')) {
                        $('#chatMessageHistoryCol').show();
                        $('#chatListCol').show();
                        $('#dashboardContainer').addClass('nonMobilePadder');
                        that.resizeChatHistory();
                    }
                }
            );
            $(window).resize(hOnWinResize);
            hOnWinResize();
        })(jQuery, ResponsiveBootstrapToolkit);
        $('#showChatDetails').click(onClickShowChatDetails);
        $('#showChatMessages').click(onClickShowChatMessages);
        $('#leaveChatBtn').click(onClickLeaveChat);
        $('#loginRegisterButton').click(function(e){e.preventDefault(); that.setView($registerView);});
        
    };
    var hNavClick = function(e)
    {
        switch ($(e.target).data('action')) {
            case 'login':
                that.setView($loginView);
                break;
            case 'logout':
                that.logout();
                break;
            case 'register':
                that.setView($registerView);
                break;
            case 'showDashboard':
                that.setView($dashboardView);
                break;
            case 'showMyProfile':
                that.setView($userProfileView);
                break;
            case 'showAdminCtl':
                that.setView($adminCtlView);
                break;
            default:
                console.warn('Non-bound action from navbar:', $(e.target).data('action'));
                break;
        }
    };
    that.login = function(user, pass, ui)
    {
        var init = function()
        {
            if(ui)
                that.loginUICoordinator(loginStates.loading);
            api_POST(dataObj, apiSuccess, apiFail);
        },
        dataObj =
        {
            q:'login',
            user:user,
            password:pass
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            if(ui)
                that.loginUICoordinator(loginStates.error, 'API Error.');
            console.error('API error');
            exit(false);
        },
        reqSuccess = function(data)
        {
            console.info('Successful request!');
            if(data.data.user !== null)
            {
                curUser = data.data.user;
                onLoggedIn();
                if(ui)
                    that.loginUICoordinator(loginStates.success);
                else
                    console.log('User \''+data.data.user.firstname+' '+data.data.user.lastname+'\' logged in.', data.data.user);
            }
            else
            {
                if(ui)
                    that.loginUICoordinator(loginStates.error, 'Wrong user/pass!');
                else
                    console.error('Wrong user/pass!');
            }
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Tuntematon virhe.';
            that.loginUICoordinator(loginStates.error, errorMsg);
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            
        };
        init();
    };
    
    var loginStates = 
    {
        loading: 0,
        error: 1,
        success: 2,
        clear: 3
    };
    
    that.loginUICoordinator = function(state, msg)
    {
        var $loadingAnim = $loginView.find('#loginLoadingRow div:eq(0)'),
            $messageArea = $loginView.find('div#loginMessage');
        
        switch(state)
        {
            case loginStates.loading:
                $loadingAnim.show();
                $loginButton.prop('disabled', true);
                $messageArea.text('Authorizing...');
                $messageArea.attr('class', 'statusMessage bg-info');
            break;
            case loginStates.error:
                $loadingAnim.hide();
                $messageArea.text(msg || 'Error logging in');
                $loginButton.prop('disabled', false);
                $messageArea.attr('class', 'statusMessage bg-danger');
            break;
            case loginStates.success:
                $loadingAnim.hide();
                $loginButton.prop('disabled', false);
                $messageArea.text('Authorized.');
                $messageArea.attr('class', 'statusMessage bg-success');
                setTimeout(function()
                {
                    that.loginUICoordinator(loginStates.clear);
                }, 200);
            break;
            case loginStates.clear:
                $loadingAnim.hide();
                $loginButton.prop('disabled', false);
                $loginView.find('input').val('');
                $messageArea.text('Provide credentials');
                $messageArea.attr('class', 'statusMessage bg-info');
            break;
        }
    };
    
    that.logout = function()
    {
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
        },
        dataObj =
        {
            q:'logout'
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            exit(false);
        },
        reqSuccess = function(data)
        {
            curUser = null;
            that.onLoggedOut();
            console.info('Successful request!');
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown';
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            
        };
        init();
    };
    
    var onUILogin = function()
    {
        that.login($inputLoginUser.val(), $inputLoginPassword.val(), true);
    };
    
    that.passChange = function(oldPass, newPass, ui)
    {
        var $loadingAnim = $('#passchangeLoadingRow div:eq(0)');
        var $statusElem = $('#passchangeMessage');
        var $submitButton = $('#passchangeButton');
        
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $submitButton.prop('disabled', true);
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'updatePassword',
            oldPass: oldPass,
            newPass: newPass
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function()
        {
            console.info('Successful request!');
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#passchangeForm input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $submitButton.prop('disabled', false);
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    var onUIPassChange = function()
    {
        var $statusElem = $('#passchangeMessage');
        if($inputPasschangeNewPass.val().trim() === '' || $inputPasschangeOldPass.val().trim() === '' || $inputPasschangeNewPassConfirm.val().trim() === '')
        {
            $statusElem.attr('class', 'statusMessage bg-danger');
            $statusElem.text('Please fill all fields.');
        }
        else if($inputPasschangeNewPass.val() !== $inputPasschangeNewPassConfirm.val())
        {
            $statusElem.attr('class', 'statusMessage bg-danger');
            $statusElem.text('Please check that the new password is entered correctly into both new password fields.');
        }
        else
            that.passChange($inputPasschangeOldPass.val(), $inputPasschangeNewPass.val(), true);
    };
    
    that.register = function(username, email, firstname, lastname, birthdate, gender, password, regToken, captcha)
    {
        var $loadingAnim = $('#registerLoadingRow div:eq(0)');
        var $statusElem = $('#registerMessage');
        var $submitButton = $('#registerButton');
        var ui = true; // Compatability
        
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $submitButton.prop('disabled', true);
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'register',
            username: username,
            email: email,
            firstname: firstname,
            lastname: lastname,
            birthdate: birthdate,
            gender: gender,
            password: password,
            regToken: regToken,
            captcha: captcha
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function()
        {
            console.info('Successful request!');
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#registerForm input, #selectRegisterGender').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                    that.setView($loginView);
                }, 1500);
            }
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            grecaptcha.reset();
            if(ui)
            {
                $submitButton.prop('disabled', false);
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    var onUIRegister = function()
    {
        var $inputRegisterUser = $('#inputRegisterUser'),
            $inputRegisterEmail = $('#inputRegisterEmail'),
            $inputRegisterFirstname = $('#inputRegisterFirstname'),
            $inputRegisterLastname = $('#inputRegisterLastname'),
            $inputRegisterBirthdateDay = $('#inputRegisterBirthdateDay'),
            $inputRegisterBirthdateMonth = $('#inputRegisterBirthdateMonth'),
            $inputRegisterBirthdateYear = $('#inputRegisterBirthdateYear'),
            $selectRegisterGender = $('#selectRegisterGender'),
            $inputRegisterPassword = $('#inputRegisterPassword'),
            $inputRegisterPasswordConfirm = $('#inputRegisterPasswordConfirm'),
            $inputRegisterRegtoken = $('#inputRegisterRegtoken');
        
        var $statusElem = $('#registerMessage');
        
        var errors = [];
        
        var emailMaxLen = 100;
        
        if($inputRegisterUser.val().trim() === '')
            errors.push({el: $inputRegisterUser, err: 'Username can\'t be empty.'});
        if($inputRegisterEmail.val().trim() === '')
            errors.push({el: $inputRegisterEmail, err: 'Email can\'t be empty.'});
        else if($inputRegisterEmail.val().trim().match(/\S+@\S+\.\S+/) === null) // string@string.string
            errors.push({el: $inputRegisterEmail, err: 'Please provide a valid email.'});
        else if($inputRegisterEmail.val().trim().length > emailMaxLen)
            errors.push({el: $inputRegisterEmail, err: 'Sorry, maximum length of email is '+emailMaxLen+' characters.'});
        if($inputRegisterFirstname.val().trim() === '')
            errors.push({el: $inputRegisterFirstname, err: 'First name can\'t be empty.'});
        if($inputRegisterLastname.val().trim() === '')
            errors.push({el: $inputRegisterLastname, err: 'Last name can\'t be empty.'});
        if($inputRegisterBirthdateDay.val().trim() === '')
            errors.push({el: $inputRegisterBirthdateDay, err: 'Birthday can\'t be empty.'});
        if($inputRegisterBirthdateMonth.val().trim() === '')
            errors.push({el: $inputRegisterBirthdateMonth, err: 'Birthmonth can\'t be empty.'});
        if($inputRegisterBirthdateYear.val().trim() === '')
            errors.push({el: $inputRegisterBirthdateYear, err: 'Birthyear can\'t be empty.'});
        if($inputRegisterBirthdateDay.val().trim() !== '' && 
            $inputRegisterBirthdateMonth.val().trim() !== '' && 
            $inputRegisterBirthdateYear.val().trim() !== '' && 
            !moment($inputRegisterBirthdateDay.val().trim()+'.'+$inputRegisterBirthdateMonth.val().trim()+'.'+$inputRegisterBirthdateYear.val().trim(), 'DD.MM.YYYY').isValid())
            errors.push({el: $inputRegisterBirthdateDay, err: 'Please enter a valid birthdate'});
        if($inputRegisterPassword.val().trim() === '')
            errors.push({el: $inputRegisterPassword, err: 'Password name can\'t be empty.'});
        else if($inputRegisterPassword.val().length < 8)
            errors.push({el: $inputRegisterPassword, err: 'Password is too short (min 8 char).'});
        if($inputRegisterPasswordConfirm.val() !== $inputRegisterPassword.val())
            errors.push({el: $inputRegisterPasswordConfirm, err: 'Passwords do not match.'});
        
        if(errors.length > 0)
        {
            $.each(errors, function(key, errorObj) {
                errorObj.el.parents('.form-group').addClass('has-error');
                errorObj.el.siblings('.personalErrorMessage').text(errorObj.err).show();
                errorObj.el.one('focus', function(e){
                    $(e.target).siblings('.personalErrorMessage').hide();
                    $(e.target).parents('.form-group').removeClass('has-error');
                });
            });
            $statusElem.attr('class', 'statusMessage bg-danger').text('Please correct the errors and resubmit.');
        }
        else
        {
            $('#registerForm .personalErrorMessage').hide();
            that.register(
                $inputRegisterUser.val().trim(), $inputRegisterEmail.val().trim(), 
                $inputRegisterFirstname.val().trim(), $inputRegisterLastname.val().trim(), 
                moment($inputRegisterBirthdateDay.val().trim()+'.'+$inputRegisterBirthdateMonth.val().trim()+'.'+$inputRegisterBirthdateYear.val().trim(), 'DD.MM.YYYY').unix(), $selectRegisterGender.val().trim(), 
                $inputRegisterPassword.val(), $inputRegisterRegtoken.val(), grecaptcha?grecaptcha.getResponse():'');
        }
    };
    
    var refreshUserProfile = function()
    {
        $('#userProfileFieldUUID').text(curUser.user_uuid);
        $('#userProfileFieldUsername').text(curUser.username);
        $('#userProfileFieldEmail').text(curUser.email);
        $('#userProfileFieldName').text(curUser.first_name+' '+curUser.last_name);
        $('#userProfileFieldBirthdate').text(moment(curUser.birthdate, 'X').format('DD.MM.YYYY'));
        $('#userProfileFieldGender').text(curUser.gender===null?'Unspecified':(parseInt(curUser.gender)===1?'Male':'Female'));
        $('#userProfileFieldRegistered').text(moment(curUser.registered, 'X').format('DD.MM.YYYY'));
        $('#userProfileFieldBanned').text(curUser.is_banned?'Yes':'No');
        $('#userProfileFieldDebugUser').text(curUser.is_debuguser?'Yes':'No');
        $('#userProfileFieldAdmin').text(curUser.is_admin?'Yes':'No');
    };
    
    var renderNameOnDashboard = function()
    {
        $dashboardView.find('.viewTitle').text('Welcome, '+curUser.first_name+' '+curUser.last_name+'!');
    };
    
    that.showRegisterView = function() {
        that.setView($registerView);
    };
    
    that.inviteNewUser = function(email)
    {
        var $loadingAnim = $('#userInviteLoadingRow div:eq(0)');
        var $statusElem = $('#userInviteMessage');
        var $submitButton = $('#userInviteButton');
        var ui = true;
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $submitButton.prop('disabled', true);
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'sendUserCreationToken',
            email: email
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function()
        {
            console.info('Successful request!');
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $submitButton.prop('disabled', false);
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    var onUIInviteNewUser = function()
    {
        var $inputInviteUserEmail = $('#inputInviteUserEmail');
        
        var $statusElem = $('#userInviteMessage');
        
        var errors = [];
        
        var emailMaxLen = 100;
        
        if($inputInviteUserEmail.val().trim() === '')
            errors.push({el: $inputInviteUserEmail, err: 'Email can\'t be empty.'});
        else if($inputInviteUserEmail.val().trim().match(/\S+@\S+\.\S+/) === null) // string@string.string
            errors.push({el: $inputInviteUserEmail, err: 'Please provide a valid email.'});
        else if($inputInviteUserEmail.val().trim().length > emailMaxLen)
            errors.push({el: $inputInviteUserEmail, err: 'Sorry, maximum length of email is '+emailMaxLen+' characters.'});
        
        if(errors.length > 0)
        {
            $.each(errors, function(key, errorObj) {
                errorObj.el.parents('.form-group').addClass('has-error');
                errorObj.el.siblings('.personalErrorMessage').text(errorObj.err).show();
                errorObj.el.one('focus', function(e){
                    $(e.target).siblings('.personalErrorMessage').hide();
                    $(e.target).parents('.form-group').removeClass('has-error');
                });
            });
            $statusElem.attr('class', 'statusMessage bg-danger').text('Please correct the errors and resubmit.');
        }
        else
        {
            $('#userInviteForm .personalErrorMessage').hide();
            that.inviteNewUser($inputInviteUserEmail.val().trim());
        }
    };
    
    that.resizeChatHistory = function()
    {
        var height = $('#chatPanel').innerHeight()-$('#chatTitle').outerHeight()-$('#chatInputControls').outerHeight();
        if($showChatList.is(':visible'))
            height -= $($showChatList.parent()[0]).outerHeight();
        $('#chatHistory').css('height', height);
        $('#chatDetails').css('height', height);
    };
    
    that.scrollDownChatHistory = function()
    {
        var $chatHistory = $('#chatHistory');
        $chatHistory.scrollTop($chatHistory[0].scrollHeight);
    };
    
    that.clearChatListUI = function()
    {
        $('#chatList').empty();
    };
    
    that.appendChatListItem = function(chatUUID, title, preview, badge, customOnClick)
    {
        var $chatElement = $('<button></button>').addClass('list-group-item').attr('data-chat-uuid', chatUUID);
        var $chatTitle = $('<h5></h5>').addClass('list-group-item-heading').text(title);
        var $chatPreview = $('<p></p>').addClass('list-group-item-text').text(preview);
        var $badge = $('<span></span>').addClass('badge').hide();
        if(typeof badge !== 'undefined')
            $badge.text(badge).show();
        $chatElement.append($badge).append($chatTitle).append($chatPreview);
        if(typeof customOnClick === 'function')
            $chatElement.click(customOnClick);
        else
            $chatElement.click(function(){
                that.activateChat($(this).data('chat-uuid'));
            });
        $('#chatList').append($chatElement);
        return $chatElement;
    };
    
    that.appendChatListItemFromData = function(chatUUID)
    {
        var maxChars = 35;
        
        var chat = chats[chatUUID];
        var title = getChatRealName(chat);
        var msg = that.getNewestMessageFromChat(chatUUID);
        
        if(msg === null)
            msg = {text: '-- No messages --'};
        var msgText = msg.text;
        
        var ellipsis = "";
        if(msgText.length > maxChars)
            ellipsis += '...';
        
        var preview = "";
        if(typeof msg.fromUser === 'undefined')
            preview = msgText;
        else
            preview = (msg.fromUser.username+': '+msgText).substring(0, maxChars)+ellipsis;
        
        that.appendChatListItem(chatUUID, title, preview);
    };
    
    var findChatListItem = function(chatUUID)
    {
        return $('#chatList button[data-chat-uuid="'+chatUUID+'"]');
    };
    
    var getChatRealName = function(chat)
    {
        if(parseInt(chat.type) === 1)
            return chat.participants[0].user_uuid === curUser.user_uuid?chat.participants[1].first_name+' '+chat.participants[1].last_name:chat.participants[0].first_name+' '+chat.participants[0].last_name;
        else
            return chat.name;
    };
    
    that.getNewestMessageFromChat = function(chatUUID)
    {
        var chat = chats[chatUUID];
        var newestMsgTimestamp = chat.newestMessageTimestamp;
        
        if(newestMsgTimestamp === null)
            return null;
        
        for(var idx = chat.messages.length-1; idx >= 0; idx--)
            if(newestMsgTimestamp === chat.messages[idx].timestamp)
                return chat.messages[idx];
        return null;
    };
    
    that.updateChatListItemFromData = function(chatUUID)
    {
        var maxChars = 35;
        
        var msg = that.getNewestMessageFromChat(chatUUID);
        
        if(msg === null)
            msg = {text: '-- No messages --'};
        
        var msgText = msg.text;
        var ellipsis = '';
        if(msgText.length > maxChars)
            ellipsis += '...';
        if(typeof msg.fromUser === 'undefined')
            that.updateChatListItem(chatUUID, getChatRealName(chats[chatUUID]), msgText);
        else
            that.updateChatListItem(chatUUID, getChatRealName(chats[chatUUID]), (msg.fromUser.username+': '+msgText).substring(0, maxChars)+ellipsis);
    };
    
    
    that.updateChatListItem = function(chatUUID, title, preview, badge)
    {
        var $chatElement = findChatListItem(chatUUID);
        var $chatTitle = $chatElement.find('.list-group-item-heading');
        var $chatPreview = $chatElement.find('.list-group-item-text');
        var $badge = $chatElement.find('.badge');
        
        if(title !== null)
            $chatTitle.text(title);
        if(preview !== null)
            $chatPreview.text(preview);
        if(typeof badge === 'undefined')
            $badge.hide();
        else
            $badge.text(badge).show();
    };
    
    that.removeChatListItem = function(chatUUID)
    {
        findChatListItem(chatUUID).remove();
    };
    
    that.getChatListItems = function()
    {
        return $('#chatList button');
    };
    
    that.getActiveChatListItems = function()
    {
        return $('#chatList button.active');
    };
    
    that.activateChat = function(chatUUID)
    {
        var $chatPanel = $('#chatPanel');
        var $chatNameEl = $chatPanel.find('#chatName');
        
        that.activateChatListItem(chatUUID);
        
        var chat = chats[chatUUID];
        
        that.clearChatMessageRows();
        
        $.each(chat.messages, function(msgIdx, message)
        {
            that.appendChatMessageRow(message.messageUUID, message.fromUser, message.timestamp, message.text);
        });
        
        (function($, viewport){

            if(viewport.is('xs')) {
                $('#chatListCol').hide();
                $('#chatMessageHistoryCol').show();
            }
        })(jQuery, ResponsiveBootstrapToolkit);
        

        $chatNameEl.text(getChatRealName(chat));
        
        $chatPanel.attr('data-active-chat-uuid', chatUUID);
        showChatMessages();
        that.renderChatDetails(chatUUID);
    };
    
    that.renderChatDetails = function(chatUUID)
    {
        $('#chatDetailsUUID').text(chats[chatUUID].chat_uuid);
        $('#chatDetailsName').text(getChatRealName(chats[chatUUID]));
        $('#chatDetailsParticipants').empty();
        $.each(chats[chatUUID].participants, function(key, participant){
            var onClick, user;
            if(participant.user_uuid !== curUser.user_uuid)
            {
                onClick = (function(){return function(){
                    that.initPrivateChat(participant.user_uuid);
                    $('#chatSearchBox').val('');
                    that.resetChatListUI();
                };})(participant.user_uuid);
                user = participant.username;
            }
            else
            {
                onClick = (function(){return function(){
                    // Do nothing
                };})(participant.user_uuid);
                user = '(You) '+participant.username;
            }
            var $listBtn = $('<button></button>')
                .addClass('list-group-item')
                .text(user)
                .click(onClick)
                .appendTo($('#chatDetailsParticipants'));
        });
        $('#leaveChatBtn').attr('data-chat-uuid', chatUUID);
        if(parseInt(chats[chatUUID].type) === 1)
        {
            $('#leaveChatBtn').hide();
            $('#chatDetailsUUIDRow').hide();
        }
        else
        {
            $('#leaveChatBtn').show();
            $('#chatDetailsUUIDRow').show();
        }
    };
    
    that.activateChatListItem = function(chatUUID)
    {
        that.getActiveChatListItems().removeClass('active');
        findChatListItem(chatUUID).addClass('active');
        $showChatList.show();
    };
    
    that.clearChatMessageRows = function()
    {
        $('#chatHistory').empty();
    };
    
    that.appendChatMessageRow = function(messageUUID, fromUser, timestamp, text)
    {
        var $chatHistory = $('#chatHistory');
        
        var $chatMessageRow = $('<div></div>').addClass('row').attr('data-message_uuid', messageUUID);
        var $chatMessageInfoCol = $('<div></div>').addClass('chatMessageInfoContainer').addClass('col-xs-3').appendTo($chatMessageRow);
        var $chatMessageTextCol = $('<div></div>').addClass('chatMessageCol').addClass('col-xs-8').appendTo($chatMessageRow);
        var $chatBubbleElem = $('<div></div>').addClass('chatBubble').appendTo($chatMessageTextCol);
        var $chatMessageTextContainer = $('<div></div>').addClass('chatMessageTextContainer').text(text).appendTo($chatBubbleElem);
        var $chatMessageTimestampElem = $('<small></small>').addClass('messageTimestamp');
        var $chatMessageFromElem = $('<small></small>').addClass('messageFrom');
        
        $chatMessageTimestampElem.text(moment(timestamp, 'X').fromNow());
        $chatMessageInfoCol.append($chatMessageTimestampElem).append($chatMessageFromElem);
        
        if(fromUser.user_uuid === curUser.user_uuid)
        {
            $chatMessageFromElem.text('You');
            $chatMessageTextCol.addClass('col-xs-offset-1').addClass('ownMessage');
        }
        else
        {
            $chatMessageFromElem.text(fromUser.username);
            $chatMessageTextCol.addClass('othersMessage');
        }
        $chatHistory.append($chatMessageRow);
        $chatHistory.scrollTop($chatHistory[0].scrollHeight);
    };
    
    that.sendNewMessageToActiveChat = function(text)
    {
        var chatUUID = $('#chatPanel').attr('data-active-chat-uuid');
        
        var newMessage = {
            messageUUID: '',
            fromUser: curUser,
            timestamp: that.serverTime(),
            text: text
        };
        
        that.sendMessage(newMessage, chatUUID);
        
        var maxChars = 35;
        
        that.appendChatMessageRow('', curUser, that.serverTime(), text);
        var ellipsis = '';
        if(text.length > maxChars)
            ellipsis += '...';
        that.updateChatListItem(chatUUID, null, (newMessage.fromUser.username+': '+text).substring(0, maxChars)+ellipsis);
    };
    
    that.onUISendMessage = function(e)
    {
        if($('#inputChatMessage').val().trim() === '')
            return false;
        that.sendNewMessageToActiveChat($('#inputChatMessage').val());
        $('#inputChatMessage').val('');
    };
    
    that.getInit = function(ui)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
            that.lastUpdateReq = that.serverTime();
        },
        dataObj =
        {
            q: 'getInit'
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            console.info('Successful request!');
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            serverTimeOffset = parseInt(data.data.serverTime)-moment().unix();
            chats = data.data.chats;
            
            setTimeout(that.getUpdates, 1500);
            
            that.renderInitChatDatas();
            
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    that.renderInitChatDatas = function()
    {
        that.clearChatMessageRows();
        that.clearChatListUI();

        var firstChat = null;

        $.each(chats, function(chatUUID, chat){
            if(firstChat === null)
                firstChat = chatUUID;
            that.appendChatListItemFromData(chatUUID);
        });
        if(firstChat !== null)
            that.activateChat(firstChat);
    };
    
    that.dumpChatDatas = function()
    {
        console.log(chats);
    };
    
    that.sendMessage = function(msgObj, chat_uuid)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;

        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'sendMessage',
            chat_uuid: chat_uuid,
            text: msgObj.text
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            console.info('Successful request!');
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            msgObj.messageUUID = data.data.message_uuid;
            
            //console.log(data);
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(!successful)
            {
                var idx = chats[chat_uuid].messages.indexOf(msgObj);
                chats[chat_uuid].messages.splice(idx, 1);
                chats[chat_uuid].newestMessageTimestamp = chats[chat_uuid].messages[chats[chat_uuid].messages.length-1].timestamp;
                that.updateChatListItemFromData(chat_uuid);
                if($('#chatPanel').data('active-chat-uuid') === chat_uuid)
                    that.activateChat(chat_uuid);
            }   
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    that.getUpdates = function()
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        /**
         * 
         * @todo don't set the lastUpdateReq if not successful.
         */
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
            that.lastUpdateReq = that.serverTime();
        },
        dataObj =
        {
            q: 'getUpdates',
            since: that.lastUpdateReq-1
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            $.each(data.data.updatedChats, function(key, chat)
            {
                $.each(chat.messages, function(key, msg){
                    
                    // Not proud of this, but the array structures in JS are not the best ones.
                    
                    var alreadyInArr = false;
                    for(var i = chats[chat.chat_uuid].messages.length - 1; i >= 0; i--)
                        if(chats[chat.chat_uuid].messages[i].messageUUID === msg.messageUUID)
                            alreadyInArr = true;
                    if(!alreadyInArr)
                    {
                        chats[chat.chat_uuid].messages.push(msg);
                        chats[chat.chat_uuid].newestMessageTimestamp = msg.timestamp;
                    }
                });
                
                if(that.getActiveChatListItems().length === 0 || that.getActiveChatListItems().data().chatUuid === chat.chat_uuid)
                    that.activateChat(chat.chat_uuid);
                that.updateChatListItemFromData(chat.chat_uuid);
            });
            $.each(data.data.newChats, function(chatUUID, chat)
            {
                if(typeof chats[chatUUID] !== 'undefined')
                    return true;
                chats[chatUUID] = chat;
                that.appendChatListItemFromData(chatUUID);
            });
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(!successful)
                setTimeout(that.getUpdates, 5000);
            else if(Math.abs(that.serverTime() - that.lastUpdateReq) < 1)
                setTimeout(that.getUpdates, 1100);
            else
                that.getUpdates();
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    that.chatSearch = function(searchText)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'chatSearch',
            searchText: searchText
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            that.clearChatListUI();
            var hasChatsWithUsers = {};
            var isInChat = {};
            $.each(chats, function(key, chat){
                if(parseInt(chat.type) === 2)
                {
                    isInChat[chat.chat_uuid] = chat.chat_uuid;
                }
                else if(parseInt(chat.type) === 1)
                {
                    hasChatsWithUsers[chat.participants[0].user_uuid] = chat.chat_uuid;
                }
            });
            var foundUsers = {};
            var foundChats = {};
            $.each(data.data.users, function(key, userInfo)
            {
                foundUsers[userInfo.user_uuid] = userInfo.user_uuid;
            });
            $.each(data.data.chats, function(key, chat)
            {
                foundChats[chat.chat_uuid] = chat.chat_uuid;
            });
            // First, add existing users to list
            $.each(hasChatsWithUsers, function(user_uuid, chat_uuid) {
                if(typeof foundUsers[user_uuid] !== 'undefined')
                    that.appendChatListItemFromData(chat_uuid);
            });
            // Second, add existing groups to list
            $.each(isInChat, function(idxUUID, chat_uuid){
                if(typeof foundChats[chat_uuid] !== 'undefined')
                    that.appendChatListItemFromData(chat_uuid);
            });
            // Third, add found users
            $.each(data.data.users, function(key, userInfo)
            {
                if(typeof hasChatsWithUsers[userInfo.user_uuid] !== 'undefined')
                    return true;
                
                var onClick = (function(userInfo){return function(){
                    that.initPrivateChat(userInfo.user_uuid);
                    $('#chatSearchBox').val('');
                    that.resetChatListUI();
                };})(userInfo);
            
                that.appendChatListItem(userInfo.user_uuid, userInfo.first_name+' '+userInfo.last_name, 'Select to start chatting!', undefined, onClick);
            });
            // Fourth, add found groupchats
            $.each(data.data.chats, function(key, chat)
            {
                if(typeof isInChat[chat.chat_uuid] !== 'undefined')
                    return true;
                
                var chatUUID = chat.chat_uuid,
                    title = chat.name,
                    preview = 'Select to join!',
                    badge = undefined;
                
                var onClick = (function(chat){return function(){
                    /*console.log("Join chat", chat);*/
                    that.joinChat(chat.chat_uuid);
                    $('#chatSearchBox').val('');
                    that.resetChatListUI();
                };})(chat);
            
                that.appendChatListItem(chatUUID, title, preview, badge, onClick);
            });
            // Lastly, add controls to make new chat
            var onClickNewPublicGroupchat = (function(chatName){return function() {
                that.newGroupChat(chatName, true);
                $('#chatSearchBox').val('');
                that.resetChatListUI();
            };})(searchText);
            
            var onClickNewHiddenGroupchat = (function(chatName){return function() {
                that.newGroupChat(chatName, false);
                $('#chatSearchBox').val('');
                that.resetChatListUI();
            };})(searchText);
            
            var $newGroupEl = that.appendChatListItem('newGroupchat', 'NEW Group', '', undefined, function(){});
            $newGroupEl.find('.list-group-item-text')
                .append($('<div></div>').css('padding-bottom', '7px').text('Create a new chat with the name you have entered in the searchbox!'))
                .append($('<button></button>').text('Public').addClass('btn').addClass('btn-primary').click(onClickNewPublicGroupchat))
                .append($('<button></button>').text('Hidden').addClass('btn').addClass('btn-default').click(onClickNewHiddenGroupchat));
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    var searchKeyDownStr = $('#chatSearchBox').val();
    
    var onSearchBoxKeydown = function()
    {
        searchKeyDownStr = $('#chatSearchBox').val();
    };
    
    that.resetChatListUI = function()
    {
        that.clearChatListUI();
        $.each(chats, function(chatUUID, chat){
            that.appendChatListItemFromData(chatUUID);
        });
        if(is_uuid($('#chatPanel').attr('data-active-chat-uuid')))
            that.activateChat($('#chatPanel').attr('data-active-chat-uuid'));
    };
    
    that.onSearchBoxKeyup = function()
    {
        if(searchKeyDownStr === $('#chatSearchBox').val())
            return false;
        var $chatSearchBox = $('#chatSearchBox');
        var text = $chatSearchBox.val();
        if(text.trim() === '')
            that.resetChatListUI();
        else if(text.trim().length < 3)
            console.log('Dismissing.');
        else
            that.chatSearch(text.trim());
    };
    
    that.initPrivateChat = function(withUserUUID)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        
        var existingChat = that.hasChatWithOtherUser(withUserUUID);
        if(existingChat !== null)
        {
            that.activateChat(existingChat);
            return;
        }
        
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'initPrivateChat',
            withUser_uuid: withUserUUID
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            chats[data.data.chat_uuid] = {
                name: data.data.withUser.first_name+' '+data.data.withUser.last_name,
                type: 1,
                participants: [
                    data.data.withUser,
                    curUser
                ],
                messages: [],
                newestMessageTimestamp: 0
            };
            that.appendChatListItemFromData(data.data.chat_uuid);
            that.activateChat(data.data.chat_uuid);
            
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    that.joinChat = function(chat_uuid)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'joinChat',
            chat_uuid: chat_uuid
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            chats[data.data.chat.chat_uuid] = {
                name: data.data.chat.name,
                type: data.data.chat.type,
                participants: data.data.chat.participants,
                messages: data.data.chat.messages,
                newestMessageTimestamp: 0
            };
            that.appendChatListItemFromData(data.data.chat.chat_uuid);
            that.activateChat(data.data.chat.chat_uuid);
            
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
                $loadingAnim.hide();
        };
        init();
    };
    
    that.newGroupChat = function(chatName, isPublic)
    {
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'newGroupChat',
            name: chatName,
            isPublic: isPublic
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            chats[data.data.chat.chat_uuid] = {
                name: data.data.chat.name,
                type: data.data.chat.type,
                participants: data.data.chat.participants,
                messages: data.data.chat.messages,
                newestMessageTimestamp: 0
            };
            that.appendChatListItemFromData(data.data.chat.chat_uuid);
            that.activateChat(data.data.chat.chat_uuid);
            
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
    };
    
    var showChatDetails = function()
    {
        $('#chatHistory').hide();
        $('#chatDetails').show();
        that.resizeChatHistory();
    };
    
    var showChatMessages = function()
    {
        $('#chatDetails').hide();
        $('#chatHistory').show();
        that.resizeChatHistory();
    };
    
    var onClickShowChatDetails = function()
    {
        showChatDetails();
    };
    
    var onClickShowChatMessages = function()
    {
        showChatMessages();
    };
    
    var onClickLeaveChat = function(e)
    {
        var chat_uuid = $(e.target).attr('data-chat-uuid');
        var $loadingAnim = $();
        var $statusElem = $();

        var ui = false;
        var init = function()
        {
            api_POST(dataObj, apiSuccess, apiFail);
            if(ui)
            {
                $statusElem.attr('class', 'statusMessage bg-info');
                $statusElem.text('Loading...');
                $loadingAnim.show();
            }
        },
        dataObj =
        {
            q: 'leaveChat',
            chat_uuid: chat_uuid
        },
        apiSuccess = function(data)
        {
            if(typeof data.nature !== 'undefined')
            {
                if(data.nature === 'S')
                    reqSuccess(data);
                else if(data.nature === 'E' || data.nature === 'W' || data.nature === 'N')
                    reqFail(data);
                else
                    apiFail(data);
            }
            else
                apiFail(data);
        },
        apiFail = function()
        {
            console.error('API error');
            if(ui)
            {
                $statusElem.text('API Error');
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            exit(false);
        },
        reqSuccess = function(data)
        {
            if(ui)
            {
                $statusElem.text('Successful!');
                $statusElem.attr('class', 'statusMessage bg-success');
                setTimeout(function(){
                    $('#inputInviteUserEmail input').val('');
                    $statusElem.text('Fill the form and press submit!');
                    $statusElem.attr('class', 'statusMessage bg-info');
                }, 1500);
            }
            
            delete chats[chat_uuid];
            that.activateChat(Object.getOwnPropertyNames(chats)[0]);
            that.resetChatListUI();
            
            exit(true);
        },
        reqFail = function(data)
        {
            var errorMsg = data.userMsg || 'Unknown error';
            if(ui)
            {
                $statusElem.text(errorMsg);
                $statusElem.attr('class', 'statusMessage bg-danger');
            }
            
            console.error(data.systemMsg);
            exit(false);
        },
        exit = function(successful)
        {
            if(ui)
            {
                $loadingAnim.hide();
            }
        };
        init();
        
    };
    
    that.hasChatWithOtherUser = function(user_uuid)
    {
        var found = null;
        $.each(chats, function(key, chat){
            if(parseInt(chat.type) === 1 && chat.participants[1].user_uuid === user_uuid)
            {
                found = chat.chat_uuid;
                return false;
            }
        });
        return found;
    };
    
    that.serverTime = function()
    {
        return moment(moment().unix()+serverTimeOffset, 'X').unix();
    };
    
    var __construct = function()
    {
        init();
    }();
    
    return __construct;
};

$(function(){
    window.sys = new System();
});