function is_uuid(str)
{
    if(typeof str !== 'string')
        return false;
    var rgx = new RegExp('^\\b[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}\\b$');
    if(str.match(rgx) === null)
        return false;
    return true;
}