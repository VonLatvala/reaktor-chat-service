<!--
 Created 3.1.2017, Axel Latvala
 UNLICENSED
-->
<?php
    require_once './inc/env_vars.php';
    require_once './inc/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <title>Reaktor Chat Service</title>
        <style>
            #followingBallsG{position:relative;width:128px;height:10px}.followingBallsG{background-color:#3D3D3D;position:absolute;top:0;left:0;width:10px;height:10px;-moz-border-radius:5px;-moz-animation-name:bounce_followingBallsG;-moz-animation-duration:2s;-moz-animation-iteration-count:infinite;-moz-animation-direction:linear;-webkit-border-radius:5px;-webkit-animation-name:bounce_followingBallsG;-webkit-animation-duration:2s;-webkit-animation-iteration-count:infinite;-webkit-animation-direction:linear;-ms-border-radius:5px;-ms-animation-name:bounce_followingBallsG;-ms-animation-duration:2s;-ms-animation-iteration-count:infinite;-ms-animation-direction:linear;-o-border-radius:5px;-o-animation-name:bounce_followingBallsG;-o-animation-duration:2s;-o-animation-iteration-count:infinite;-o-animation-direction:linear;border-radius:5px;animation-name:bounce_followingBallsG;animation-duration:2s;animation-iteration-count:infinite;animation-direction:linear}#followingBallsG_1{-moz-animation-delay:0s;-webkit-animation-delay:0s;-ms-animation-delay:0s;-o-animation-delay:0s;animation-delay:0s}#followingBallsG_2{-moz-animation-delay:.2s;-webkit-animation-delay:.2s;-ms-animation-delay:.2s;-o-animation-delay:.2s;animation-delay:.2s}#followingBallsG_3{-moz-animation-delay:.4s;-webkit-animation-delay:.4s;-ms-animation-delay:.4s;-o-animation-delay:.4s;animation-delay:.4s}#followingBallsG_4{-moz-animation-delay:.6s;-webkit-animation-delay:.6s;-ms-animation-delay:.6s;-o-animation-delay:.6s;animation-delay:.6s}@-moz-keyframes bounce_followingBallsG{0%{left:0;background-color:#3D3D3D}50%{left:118px;background-color:#CDCFE4}100%{left:0;background-color:#3D3D3D}}@-webkit-keyframes bounce_followingBallsG{0%{left:0;background-color:#3D3D3D}50%{left:118px;background-color:#CDCFE4}100%{left:0;background-color:#3D3D3D}}@-ms-keyframes bounce_followingBallsG{0%{left:0;background-color:#3D3D3D}50%{left:118px;background-color:#CDCFE4}100%{left:0;background-color:#3D3D3D}}@-o-keyframes bounce_followingBallsG{0%{left:0;background-color:#3D3D3D}50%{left:118px;background-color:#CDCFE4}100%{left:0;background-color:#3D3D3D}}@keyframes bounce_followingBallsG{0%{left:0;background-color:#3D3D3D}50%{left:118px;background-color:#CDCFE4}100%{left:0;background-color:#3D3D3D}}
        </style>
        <link rel="stylesheet" href="css/jquery-ui-1.10.0.custom.css">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/app.css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./">Reaktor Chat Service</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav nav-loggedout">
                        <li class="showOnLoggedOut">
                            <a href="#" id="navLoginBtn" data-action="login" data-toggle="collapse" data-target=".navbar-collapse.in">Login</a>
                        </li>
                        <li class="showOnLoggedOut">
                            <a href="#" id="navRegisterBtn" data-action="register" data-toggle="collapse" data-target=".navbar-collapse.in">Register</a>
                        </li>
                        <li class="showOnLoggedIn">
                            <a href="#" id="navDashboard" data-action="showDashboard" data-toggle="collapse" data-target=".navbar-collapse.in">Dashboard</a>
                        </li>
                        <li class="showOnLoggedIn">
                            <a href="#" id="navUserButton" data-action="showMyProfile" data-toggle="collapse" data-target=".navbar-collapse.in">User Profile</a>
                        </li>
                        <li class="showOnJSActivation">
                            <a href="#" id="navAdminButton" data-action="showAdminCtl" data-toggle="collapse" data-target=".navbar-collapse.in">Admin Control Panel</a>
                        </li>
                        <li class="showOnLoggedIn">
                            <a href="#" id="navLogoutBtn" data-action="logout" data-toggle="collapse" data-target=".navbar-collapse.in">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="app" class="container">
            <div id="loadingView" class="view">
                <h1 class="viewTitle text-center">
                    Loading...
                </h1>
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-xs-12">
                        <div id="followingBallsG" style="margin: 0 auto;">
                            <div id="followingBallsG_1" class="followingBallsG">
                            </div>
                            <div id="followingBallsG_2" class="followingBallsG">
                            </div>
                            <div id="followingBallsG_3" class="followingBallsG">
                            </div>
                            <div id="followingBallsG_4" class="followingBallsG">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="view" id="loginView" style="display: none;">
                <h1 class="viewTitle">
                    Login
                </h1>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">
                            Login Form
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="row form-group">
                                <label for="inputLoginUser" class="col-xs-4 col-sm-2 control-label">User</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputLoginUser" placeholder="User / Email" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputLoginPassword" class="col-xs-4 col-sm-2 control-label">Password</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputLoginPassword" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <button class="btn btn-primary" id="loginButton">Login</button>
                                    <button class="btn btn-default" id="loginRegisterButton">Or Register</button>
                                </div>
                            </div>
                            <div class="row" id="loginMessageRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <div id="loginMessage" class="statusMessage bg-info">
                                        Provide credentials
                                    </div>
                                </div>
                            </div>
                            <div class="row loadingRow" id="loginLoadingRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10" style="display: none;">
                                    <div class="loadingAnimation" id="followingBallsG">
                                        <div id="followingBallsG_1" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_2" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_3" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_4" class="followingBallsG">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <div class="view" id="registerView" style="display: none;">
                <h1 class="viewTitle">
                    Register
                </h1>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Registration Form
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="registerForm">
                            <?php if(REGTOKEN_REQUIRED) { ?>
                            <div class="row form-group">
                                <label for="inputRegisterRegtoken" class="col-xs-4 col-sm-2 control-label">Registration Token</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputRegisterRegtoken" placeholder="Registration Token" value="<?php if(isset($_GET['regToken'])) echo $_GET['regToken']; ?>">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row form-group">
                                <label for="inputRegisterUser" class="col-xs-4 col-sm-2 control-label">Username</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputRegisterUser" placeholder="Username">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterEmail" class="col-xs-4 col-sm-2 control-label">Email</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputRegisterEmail" placeholder="Email">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterFirstname" class="col-xs-4 col-sm-2 control-label">First Name</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputRegisterFirstname" placeholder="First Name">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterLastname" class="col-xs-4 col-sm-2 control-label">Last Name</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputRegisterLastname" placeholder="Last Name">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterBirthdateDay" class="col-xs-4 col-sm-2 control-label">Birthdate</label>
                                <div class="col-xs-8 col-xs-offset-4 col-sm-offset-0 col-sm-3">
                                    <input type="text" class="form-control" id="inputRegisterBirthdateDay" placeholder="Day">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                                <div class="col-xs-8 col-xs-offset-4 col-sm-offset-0 col-sm-3">
                                    <input type="text" class="form-control" id="inputRegisterBirthdateMonth" placeholder="Month">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                                <div class="col-xs-8 col-xs-offset-4 col-sm-offset-0 col-sm-4">
                                    <input type="text" class="form-control" id="inputRegisterBirthdateYear" placeholder="Year">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <label for="selectRegisterGender" class="col-xs-4 col-sm-2 control-label">Gender</label>
                                <div class="col-xs-8 col-sm-10">
                                    <select id="selectRegisterGender" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                    </select>
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterPassword" class="col-xs-4 col-sm-2 control-label">Password</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputRegisterPassword" placeholder="Password">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputRegisterPasswordConfirm" class="col-xs-4 col-sm-2 control-label">Password (Confirm)</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputRegisterPasswordConfirm" placeholder="Password (Confirm)">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <?php if(CAPTCHA_REQUIRED) {; ?>
                            <div class="row form-group">
                                <label class="col-xs-4 col-sm-2 control-label">Human or Robot?</label>
                                <div class="col-xs-12 col-sm-10 text-center" style="overflow: hidden;">
                                    <div class="g-recaptcha" id="recaptchaRegister" data-sitekey="6Lc_lBAUAAAAAGW2w7Szwyn1r9FJUCW1XD88xBKK"></div>
                                </div>
                            </div>
                            <?php }; ?>
                            <div class="row form-group">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <button class="btn btn-primary" id="registerButton">Register</button>
                                </div>
                            </div>
                            <div class="row" id="registerMessageRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <div id="registerMessage" class="statusMessage bg-info">
                                        Please fill the form and press register!
                                    </div>
                                </div>
                            </div>
                            <div class="row loadingRow" id="registerLoadingRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10" style="display: none;">
                                    <div class="loadingAnimation" id="followingBallsG">
                                        <div id="followingBallsG_1" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_2" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_3" class="followingBallsG">
                                        </div>
                                        <div id="followingBallsG_4" class="followingBallsG">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <div class="view" id="userProfileView" style="display: none;">
                <h1 class="viewTitle">
                    User Profile
                </h1>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">
                            User Details
                        </h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                User UUID
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldUUID">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Username
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldUsername">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Email
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldEmail">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Name
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldName">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Birthdate
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldBirthdate">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Gender
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldGender">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Registered
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldRegistered">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Banned?
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldBanned">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Debug user?
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldDebugUser">
                                Text
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                Admin?
                            </div>
                            <div class="col-xs-8 col-sm-10" id="userProfileFieldAdmin">
                                Text
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">
                            Change Password
                        </h2>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="passchangeForm">
                            <div class="row form-group">
                                <label for="inputPasschangeOldPass" class="col-xs-4 col-sm-2 control-label">Old Password</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputPasschangeOldPass" placeholder="Old Password">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputPasschangeNewPass" class="col-xs-4 col-sm-2 control-label">New Password</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputPasschangeNewPass" placeholder="New Password">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label for="inputPasschangeNewPassConfirm" class="col-xs-4 col-sm-2 control-label">New Password (Confirm)</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="password" class="form-control" id="inputPasschangeNewPassConfirm" placeholder="New Password (Confirm)">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <button class="btn btn-primary" id="passchangeButton">Submit</button>
                                </div>
                            </div>
                            <div class="row" id="passchangeMessageRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <div id="passchangeMessage" class="statusMessage bg-info">
                                        Fill the form and press submit!
                                    </div>
                                </div>
                            </div>
                            <div class="row loadingRow" id="passchangeLoadingRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10" style="display: none;">
                                    <div class="loadingAnimation">
                                        <div id="followingBallsG">
                                            <div id="followingBallsG_1" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_2" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_3" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_4" class="followingBallsG">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="view fill" id="dashboardView" style="display: none;">
                <h1 class="viewTitle hidden-xs">
                    Welcome, {{Username}}!
                </h1>
                <div id="dashboardContainer" class="row fill">
                    <div class="col-xs-12 col-sm-4 fill" id="chatListCol">
                        <!-- left part of dashboard with own chats list and chat search -->
                        <div class="panel panel-default fill">
                            <div class="panel-heading">
                                <div class="input-group">
                                    <input class="form-control" id="chatSearchBox" placeholder="Search for users and chats">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" id="clearChatSearchBtn"><span class="glyphicon glyphicon-remove-circle"></span></button>
                                    </span>
                                </div>
                            </div>
                            <div style="height: 100%; padding-bottom: 55px;">
                                <ul class="list-group fill autoscroll-y" id="chatList">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 fill" id="chatMessageHistoryCol">
                        <!-- right part of dashboard with the chatting interface -->
                        <div class="panel panel-default fill" id="chatPanel">
                            <div class="hidden-sm hidden-md hidden-lg panel-heading">
                                <button class="btn btn-default" id="showChatlist">
                                    Show chatlist
                                </button>
                            </div>
                            <div class="panel-heading" id="chatTitle">
                                <h3 class="panel-title pull-left" id="chatName">
                                    
                                </h3>
                                <button type="button" class="btn btn-default panel-title pull-right" id="showChatDetails">
                                    <span class="glyphicon glyphicon-option-vertical"></span>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body" id="chatHistory">
                                
                            </div>
                            <div class="panel-body" id="chatDetails" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="btn btn-default" id="showChatMessages">
                                            <span class="glyphicon glyphicon-arrow-left"></span> Back
                                        </button>
                                    </div>
                                </div>
                                <div class="row" id="chatDetailsUUIDRow">
                                    <div class="col-xs-12 col-sm-2">
                                        <h4>UUID</h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-10 padTop" id="chatDetailsUUID"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2">
                                        <h4>Name</h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-10 padTop" id="chatDetailsName"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2">
                                        <h4>Participants</h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-10 padTop">
                                        <ul class="list-group" id="chatDetailsParticipants">
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                        <button class="btn btn-danger" id="leaveChatBtn">Leave chat</button>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" id="chatInputControls">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="inputChatMessage">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" id="sendNewChatMessageBtn">
                                            <span class="glyphicon glyphicon-send"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="view" id="adminCtlView" style="display: none;">
                <h1 class="viewTitle">
                    Admin Control Panel
                </h1>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Actions
                        </h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>Ban stuff</li>
                            <li>Invite people</li>
                            <li>Promote people</li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Invite Users
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="userInviteForm">
                            <div class="row form-group">
                                <label for="inputInviteUserEmail" class="col-xs-4 col-sm-2 control-label">Email</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="inputInviteUserEmail" placeholder="Email">
                                    <div class="personalErrorMessage bg-danger" style="display: none;"></div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <button class="btn btn-primary" id="userInviteButton">Invite</button>
                                </div>
                            </div>
                            <div class="row" id="userInviteMessageRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10">
                                    <div id="userInviteMessage" class="statusMessage bg-info">
                                        Fill the form and press invite!
                                    </div>
                                </div>
                            </div>
                            <div class="row loadingRow" id="userInviteLoadingRow">
                                <div class="col-xs-offset-4 col-sm-offset-2 col-xs-8 col-sm-10" style="display: none;">
                                    <div class="loadingAnimation">
                                        <div id="followingBallsG">
                                            <div id="followingBallsG_1" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_2" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_3" class="followingBallsG">
                                            </div>
                                            <div id="followingBallsG_4" class="followingBallsG">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            User Manager
                        </h3>
                    </div>
                    <div class="panel-body" id="userManagerPanelBody">
                        
                    </div>
                </div>
            </div>
        </div>
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/responsive-toolkit/dist/bootstrap-toolkit.min.js"></script>
        <script src="node_modules/moment/min/moment-with-locales.min.js"></script>
        <script src="js/api_POST.min.js"></script>
        <script src="js/lib.js"></script>
        <script src="js/runtime.js"></script>
        <?php if(ENABLE_GOOGLE_ANALYTICS) { ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', '<?php echo GA_TRACKING_ID; ?>', 'auto');
            ga('send', 'pageview');
        </script>
        <?php }?>
        <?php if(isset($_GET['regToken']) || isset($_GET['register'])) echo '<script>$(function(){sys.requestRegView = true;});</script>'; ?>
    </body>
</html>