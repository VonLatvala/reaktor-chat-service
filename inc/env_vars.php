<?php
    define('APACHE_PATH', DIRECTORY_SEPARATOR."sites".DIRECTORY_SEPARATOR."fi-alatvala-reaktor-chat-service");
    define('PROJECT_PATH', APACHE_PATH.DIRECTORY_SEPARATOR."public_html");
    define('INC_DIR', PROJECT_PATH.DIRECTORY_SEPARATOR.'inc');
    define('CLASS_DIR', INC_DIR.DIRECTORY_SEPARATOR."classes");