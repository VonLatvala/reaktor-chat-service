<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Reaktor Chat Service - New user creation</title>
        <link href="/inc/html/newUserCreationMail/css.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div id='content'>
            <h3>
                Greetings!
            </h3>
            <p class="lead">
                You have received this message because you have been granted the right to register a user to the Reactor Chat Service.
            </p>
            <p>
                We have created a registration token for you to use in the registration process.
                Please navigate to <a href="https://reaktor-chat-service.alatvala.fi/?register&regToken=<?php echo $regToken; ?>">https://reaktor-chat-service.alatvala.fi/?register&regToken=<?php echo $regToken; ?></a>
                to complete the registration process, and in case your token isn't automatically inserted to the form, use the bold one here: <strong><?php echo $regToken; ?></strong>
            </p>
            <p>
                Hope to see you!
            </p>
        </div>
    </body>
</html>
