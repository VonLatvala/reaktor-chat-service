<?php
    require_once APACHE_PATH.DIRECTORY_SEPARATOR.'passwords.php';
    
    define('APP_DATABASE_HOST', 'localhost');
    define('APP_DATABASE_USER', 'REAKTOR_CHAT_SYS');
    define('APP_DATABASE_NAME', 'reaktor_chat_service_app');
    
    define('LOGIN_DATABASE_HOST', 'localhost');
    define('LOGIN_DATABASE_USER', 'REAKTOR_CHAT_LOG');
    define('LOGIN_DATABASE_NAME', 'reaktor_chat_service_login');
    
    /**
     * DANGEROUS to log all queries. Use only in dire situations. Passwords and other sensitive information
     * will probably show up in the querylog. So if you use it, please do chmod the file 600.
     */
    define('MYSQL_WRAPPER_QUERYLOG', false);
    
    define('RECAPTCHA_SITE_VERIFY_URL', 'https://www.google.com/recaptcha/api/siteverify');
    
    define('REAKTOR_CHAT_SERVICE_MIN_LEN_PASSWORD', 8);
    define('REAKTOR_CHAT_SERVICE_MIN_LEN_USERNAME', 3);
    
    define('LOCALE', 'en_US');
    
    define('REGTOKEN_REQUIRED', false);
    define('CAPTCHA_REQUIRED', true);
    define('ENABLE_GOOGLE_ANALYTICS', true);
    
    define('CHAT_CONFIG_NUM_MESSAGES_ON_INIT', 50);