<?php
    /**
     * Description of chat
     *
     * Created Jan 5, 2017 10:43:41 PM GMT+2
     * @author Axel Latvala
     */
    class Chat
    {
        const TYPE_PRIVATE = 1;
        const TYPE_GROUP = 2;
        
        /**
         * Textual `chat_uuid`
         * @var string 
         */
        public $chat_uuid;
        /**
         * Chat name
         * @var string 
         */
        public $name;
        /**
         * -128 - 127, the type of chat.
         * @var int 
         */
        public $type;
        /**
         * Wether or not the chat is visible for non-participants
         * @var boolean 
         */
        public $is_public;
        
        /**
         *
         * @var type 
         */
        public $messages = [];

        public function __construct($chat_uuid, $name, $type, $is_public)
        {
            $this->chat_uuid = $chat_uuid;
            $this->name = $name;
            $this->type = $type;
            $this->is_public = $is_public;
        }

    }