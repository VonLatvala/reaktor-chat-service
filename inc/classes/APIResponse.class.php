<?php
    /**
     * An element returned by the API to the client.
     *
     * Created 17.1.2015 12:16:58 GMT+2
     * @author Axel Latvala
     */
    class APIResponse
    {
        /* @var $nature string S = Success, E = Error, W = Warning N = Notice */
        /* @var $systemCode int The response error/warning/notice code */
        /* @var $systemMsg string Message to be used by the system */
        /* @var $userMsg string Message to be shown to the user */
        /* @var $data array Array containing data */
        /* @var $debug Array Array containing debug info */
        private $nature = 'E',
                $systemCode = null,
                $systemMsg = 'E_UNKNOWN',
                $userMsg,
                $data = [];
        public
                $debug = [];
        
        /* @var $i18n array Array with translations */
        public static $i18n = 
        [
            'en_US' =>
            [
                'E_UNKNOWN' => 'Unknown error.',
                'E_UNEXPECTED' => 'Unexpected error.',
                'W_UNEXPECTED' => 'Unexpected warning.',
                'N_UNEXPECTED' => 'Unexpected notice.',
                'E_UNSPECIFIED_COMMAND' => 'Unspecified command.',
                'E_QUERY' => 'Error querying the database.',
                'E_APPLICATIONLOAD' => 'Error loading application.'
            ],
            'fi_FI' =>
            [
                'E_UNKNOWN' => 'Tuntematon virhe.',
                'E_UNEXPECTED' => 'Odottamaton virhe.',
                'W_UNEXPECTED' => 'Odottamaton varoitus.',
                'N_UNEXPECTED' => 'Odottamaton huomautus.',
                'E_UNSPECIFIED_COMMAND' => 'Rajapintapyynnössä ei komentoa / väärä komento.',
                'E_QUERY' => 'Vika tietokantakyselyssä.',
                'E_APPLICATIONLOAD' => 'Vika ladattaessa hakemusta.'
            ],
            'sv_FI' =>
            [
                'E_UNKNOWN' => 'Okänt fel.',
                'E_UNEXPECTED' => 'Oförväntat fel.',
                'W_UNEXPECTED' => 'Oförväntad varning.',
                'N_UNEXPECTED' => 'Odottamaton tillkännegivelse.',
                'E_UNSPECIFIED_COMMAND' => 'Gränssnittförfrågan hade inget kommand / fel kommand.',
                'E_QUERY' => 'Fel vid databassökning.',
                'E_APPLICATIONLOAD' => 'Fel vid laddning av ansökning.'
            ]
        ];
        
        public function __construct()
        {
            $this->userErrorMsg = APIResponse::$i18n[LOCALE]['E_UNKNOWN'];
        }
        
        /**
         * Sets the response to represent an error.
         * @param string $sysMsg
         * @param string $userMsg
         * @param int $code
         */
        public function setError($sysMsg, $userMsg = null, $code = null)
        {
            if($userMsg === null)
                $userMsg = APIResponse::$i18n[LOCALE]['E_UNEXPECTED'];
            if($code === null)
                $code = 500;
            $this->systemMsg = $sysMsg;
            $this->systemCode = $code;
            $this->userMsg = $userMsg;
            $this->nature = 'E';
        }
        
        /**
         * Sets the response to represent a warning.
         * @param string $sysMsg
         * @param string $userMsg
         * @param int $code
         */
        public function setWarn($sysMsg, $userMsg = null, $code = null)
        {
            if($userMsg === null)
                $userMsg = APIResponse::$i18n[LOCALE]['W_UNEXPECTED'];
            if($code === null)
                $code = 500;
            $this->systemMsg = $sysMsg;
            $this->systemCode = $code;
            $this->userMsg = $userMsg;
            $this->nature = 'W';
        }
        
        /**
         * Sets the response to represent a warning.
         * @param string $sysMsg
         * @param string $userMsg
         * @param int $code
         */
        public function setNotice($sysMsg, $userMsg = null, $code = null)
        {
            if($userMsg === null)
                $userMsg = APIResponse::$i18n[LOCALE]['N_UNEXPECTED'];
            if($code === null)
                $code = 500;
            $this->systemMsg = $sysMsg;
            $this->systemCode = $code;
            $this->userMsg = $userMsg;
            $this->nature = 'N';
        }
        
        /**
         * Sets the response to represent a success.
         */
        public function setSuccess()
        {
            $this->nature = 'S';
        }
        
        /**
         * Sets the response data.
         * @param array $data
         */
        public function setData($data)
        {
            $this->data = $data;
        }

        public function __toString()
        {
            return $this->toJSON($this->toArray());
        }
        
        public function toArray()
        {
            return 
            [
                'nature' => $this->nature,
                'systemCode' => $this->systemCode,
                'systemMsg' => $this->systemMsg,
                'userMsg' => $this->userMsg,
                'data' => $this->data,
                'debug' => $this->debug
            ];
        }
        
        public function toJSON()
        {
            return json_encode($this->toArray());
        }
    }