<?php
    /**
     * Class to hold utility functions.
     *
     * @author Axel Latvala
     * @version 1.0
     * @static
     * Created  31.10.2012 22:48 GMT+2
     * Modified 12.11.2012 02:04 GMT+2, Axel Latvala 
     * 
     * 27.06.2014 FROM eTunti
     * 
     */
    class utility
    {
        public function __construct()
        {

        }
        
        public static function is_uuid($str)
        {
            $pattern = "/\b[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}\b/";
            if(preg_match($pattern, $str))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static function generate_uuid()
        {
            $DB = new mysql_DB(APP_DATABASE_HOST, APP_DATABASE_USER, APP_DATABASE_PASSWORD, APP_DATABASE_NAME);
            
            if(!$DB->is_connected())
            {
                trigger_error("Could not connect to the database. <br />\nMySQL(".$DB->error_code()."): ".$DB->error_str(), E_USER_ERROR);
                die();
            }
            
            if($DB->query("SELECT CONV_BIN_TXT_UUID(GEN_BIN_UUID()) as UUID"))
            {
                $resarr = $DB->result->fetch_assoc(); 
                $DB->result->close();
                $DB->disconnect();
                
                return $resarr['UUID'];
            }
            else
            {
                echo $DB->error_str();
                return false;
            }
        }
        
        public static function is_valid_url($str)
        {
            return (bool)parse_url($str);
        }

        public static function is_url_reachable($url)
        {
            if(!self::is_valid_url($url))
            {
                return false;
            }
            
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_NOBODY, TRUE);
            $data = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            
            if($httpcode>=200 && $httpcode<300)
            {
                return true;
            }
            else
            {
                return false;
            }  
        }
        
        public static function url_info($url)
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,            $url);
            curl_setopt($ch, CURLOPT_HEADER,         true);
            curl_setopt($ch, CURLOPT_NOBODY,         true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT,        10);

            $r = curl_exec($ch);
            
            $retVal = array();
            $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $r));
            foreach( $fields as $field )
            {
                if( preg_match('/([^:]+): (.+)/m', $field, $match) )
                {
                    $match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
                    if( isset($retVal[$match[1]]) )
                    {
                        $retVal[$match[1]] = array($retVal[$match[1]], $match[2]);
                    }
                    else
                    {
                        $retVal[$match[1]] = trim($match[2]);
                    }
                }
            }
            
            $res['header'] = $retVal;
            return $res;
        }

        public static function is_valid_email_address($str)
        {
            return filter_var($str, FILTER_VALIDATE_EMAIL);
        }
        
        public static function without_spaces($str)
        {
            return implode('', explode(' ', $str));
        }
        
        public static function is_valid_timestamp($int)
        {
            $int = (string)$int;
            return ((string) (int) $int === $int) 
                && ($int <= PHP_INT_MAX)
                && ($int >= ~PHP_INT_MAX);
        }

        public static function random_string($chars)
        {
            $res = "";
            for($i=1;$i<=$chars;$i++)
            {
                $res .= mb_convert_encoding(pack('n', rand(0x61,0x7A)), 'UTF-8', 'UTF-16BE');
            }
            return $res;
        }

        public static function property(&$properties, $property, $bool = null)
        {
            // Mode describes if we are reading or writing. 0 = read, 1 = write.
            if($bool === null)
            {
                $mode = 0;
            }
            else
            {
                $mode = 1;
            }
            if($mode == 1)
            {
                if($bool === false)
                {
                    $replacement = "0";
                }
                else if($bool === true)
                {
                    $replacement = "1";
                }
                else
                {
                    // Invalid input.
                    return false;
                }
                $properties = substr_replace($properties, $replacement, $property, 1);
                return true;
            }
            else
            {
                return substr($properties, $property, 1);
            }
        }
        
        public static function could_be_int($mixed)
        {
            if(empty($mixed))
            {
                return false;
            }
            if((int)$mixed == $mixed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static function could_be_float($mixed)
        {
            if(empty($mixed))
            {
                return false;
            }
            if((float)$mixed == $mixed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static function could_be_double($mixed)
        {
            if(empty($mixed))
            {
                return false;
            }
            if((double)$mixed == $mixed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public static function cast_boolean(&$mixed)
        {
            if($mixed === 1 || $mixed === "1" || strtoupper($mixed) === "TRUE" || $mixed === true)
            {
                $mixed = true;
            }
            elseif($mixed === 0 || $mixed === "0" || strtoupper($mixed) === "FALSE" || $mixed === false)
            {
                $mixed = false;
            }
            else
            {
                $mixed = (bool)$mixed;
            }
        }
        
        public static function is_ean($barcode)
        {
            // check to see if barcode is 13 digits long
            // edit: 13 OR 8 long
            if (!preg_match("/^\b[0-9]{13}\b|\b[0-9]{8}\b$/", $barcode))
            {
                return false;
            }

            // edit: Fill with zeros in case of EAN8

            $digits = $barcode;

            $digits = substr(-13,13,('00000'.$digits));

            // 1. Add the values of the digits in the 
            // even-numbered positions: 2, 4, 6, etc.
            $even_sum = $digits[1] + $digits[3] + $digits[5] +
                        $digits[7] + $digits[9] + $digits[11];

            // 2. Multiply this result by 3.
            $even_sum_three = $even_sum * 3;

            // 3. Add the values of the digits in the 
            // odd-numbered positions: 1, 3, 5, etc.
            $odd_sum = $digits[0] + $digits[2] + $digits[4] +
                       $digits[6] + $digits[8] + $digits[10];

            // 4. Sum the results of steps 2 and 3.
            $total_sum = $even_sum_three + $odd_sum;

            // 5. The check character is the smallest number which,
            // when added to the result in step 4, produces a multiple of 10.
            $next_ten = (ceil($total_sum / 10)) * 10;
            $check_digit = $next_ten - $total_sum;

            // if the check digit and the last digit of the 
            // barcode are OK return true;
            if ($check_digit == $digits[12]) {
                return true;
            }

            return false;
        }
        
        /** 
         * Convert number of seconds into hours, minutes and seconds 
         * and return an array containing those values 
         * 
         * @param integer $inputSeconds Number of seconds to parse 
         * @return array 
         */ 

        public static function secondsToTime($inputSeconds)
        {
            $secondsInAMinute = 60;
            $secondsInAnHour  = 60 * $secondsInAMinute;
            $secondsInADay    = 24 * $secondsInAnHour;

            // extract days
            $days = floor($inputSeconds / $secondsInADay);

            // extract hours
            $hourSeconds = $inputSeconds % $secondsInADay;
            $hours = floor($hourSeconds / $secondsInAnHour);

            // extract minutes
            $minuteSeconds = $hourSeconds % $secondsInAnHour;
            $minutes = floor($minuteSeconds / $secondsInAMinute);

            // extract the remaining seconds
            $remainingSeconds = $minuteSeconds % $secondsInAMinute;
            $seconds = ceil($remainingSeconds);

            // return the final array
            $obj = array(
                'd' => (int) $days,
                'h' => (int) $hours,
                'm' => (int) $minutes,
                's' => (int) $seconds,
            );
            return $obj;
        }

        public static function is_ssn($str)
        {
            $pattern = '/(^[0-2][0-9]|3[0-1])(0[0-9]|1[0-2])([0-9][0-9])([+A-])([[:digit:]]{3})([A-Z]|[[:digit:]])$/i';
            $result = array();
            if(preg_match($pattern, $str, $result))
            {
                $day = (int)$result[1];
                $month = (int)$result[2];
                if($result[4]=='+')
                    $century = '18';
                if($result[4]=='-')
                    $century = '19';
                if($result[4]=='A')
                    $century = '20';
                $vuosi = $century.$result[3];
                $year = (int)$vuosi;
                if(checkdate($month, $day, $year))
                {
                    $numbers = $result[1].$result[2].$result[3].$result[5];
                    $number = (int)$numbers;
                    $remainer = $number%31;
                    $list = array(10 => 'A', 11 => 'B', 12 => 'C', 13 => 'D', 14 => 'E', 15 => 'F', 16 => 'H', 17 => 'J', 18 => 'K', 19 => 'L',
                        20 => 'M', 21 => 'N', 22 => 'P', 23 => 'R', 24 => 'S', 25 => 'T', 26 => 'U', 27 => 'V', 28 => 'W', 29 => 'X', 30 => 'Y');
                    if($remainer<10)
                        $checksum = $remainer;
                    else
                        $checksum = $list[$remainer];
                    if($result[6]==$checksum)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;

        }

        public static function strip_all_whitespace($str)
        {
            return preg_replace('/\s+/', '', $str);
        }
        
        public static function calculate_pankkiviite_chksum($inStr)
        {
            $mult = array(7, 3, 1);
            $sum = 0;
            $seed = (string)$inStr;
            
            for($i = strlen($seed)-1; $i>=0; $i--)
            {
                $sum += $seed[$i] * $mult[((strlen($seed)-1)-$i)%3];
            }
            $strSum = (string)$sum;
            $sub = 10-((int)($strSum[strlen($strSum)-1]))+$strSum;
            return substr((string)($sub-$sum), -1);
        }
        
        public static function verify_viite($inRef) {
            $viite = utility::strip_all_whitespace($inRef);
            $nochk = substr($viite, 0, strlen($viite)-1);
            $suppliedChk = substr($viite, -1);
            if($suppliedChk === utility::calculate_pankkiviite_chksum($nochk))
            {
                return true;
            }
            return false;
        }
        
        public static function char_pad($str, $char, $length)
        {
            $strlen = strlen($str);
            $pads = $length - $strlen;
            for($i = 0; $i < $pads; $i++)
                $str = $char[0].$str;
            return $str;
        }
    }