<?php
    /**
     * Description of chat_message
     *
     * Created Jan 5, 2017 10:42:32 PM GMT+2
     * @author Axel Latvala
     */
    class ChatMessage
    {
        /**
         *
         * @var string 
         */
        public $message_uuid;
        /**
         *
         * @var string
         */
        public $user_uuid;
        /**
         *
         * @var string
         */
        public $chat_uuid;
        /**
         *
         * @var int
         */
        public $timestamp;
        /**
         *
         * @var string
         */
        public $text;
        /**
         *
         * @var boolean
         */
        public $is_removed;
        
        /**
         * 
         * @param string $message_uuid `message_uuid` textual uuid
         * @param string $user_uuid `user_uuid` textual uuid
         * @param string $chat_uuid `chat_uuid` textual uuid
         * @param int $timestamp timestamp of when the message was submitted
         * @param string $text the message
         * @param bool $is_removed wether it's removed or not
         */
        public function __construct($message_uuid, $user_uuid, $chat_uuid, $timestamp, $text, $is_removed)
        {
            $this->message_uuid = $message_uuid;
            $this->user_uuid = $user_uuid;
            $this->chat_uuid = $chat_uuid;
            $this->timestamp = $timestamp;
            $this->text = $text;
            $this->is_removed = $is_removed;
        }

    }
