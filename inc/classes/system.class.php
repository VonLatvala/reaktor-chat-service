<?php
    
    /**
     * Pretty much the API
     *
     * @author Axel Latvala
     */
    
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
    require_once INC_DIR.DIRECTORY_SEPARATOR.'config.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'utility.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'mysql_db.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'user.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'ChatMessage.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'Chat.class.php';
    require_once CLASS_DIR.DIRECTORY_SEPARATOR.'ChatSystem.class.php';
    
    class System
    {
        /**
         *
         * @var User 
         */
        private $userRef;
        /**
         *
         * @var mysql_DB 
         */
        private $masterHandle = null;
        
        public function __construct()
        {
            session_start();
            $mysql_db = $this->getAppMySQLHandle();
            $this->masterHandle = $mysql_db;
            if(!isset($_SESSION['user_uuid']))
            {
                $_SESSION['user_uuid'] = NULL;
            }
            $this->userRef = new User($_SESSION['user_uuid'], $mysql_db, $this->getLoginMySQLHandle());
            if($this->userRef->is_banned())
            {
                $this->userRef->log_out();
                trigger_error("User banned", E_USER_ERROR);
            }
        }
        
        public function user_ref()
        {
            return $this->userRef;
        }
        
        public function run_API()
        {
            $apiResponse = new APIResponse();
            if($_SERVER['REQUEST_METHOD'] === 'POST')
            {
                // POST request
                switch($_POST['q'])
                {
                    case 'register':
                        $this->apiRegister($apiResponse);
                        break;
                    case 'login':
                        $this->apiLogin($apiResponse);
                        break;
                    case 'logout':
                        $this->apiLogout($apiResponse);
                        break;
                    case 'loginStatus':
                        $this->apiLoginStatus($apiResponse);
                        break;
                    case 'sendUserCreationToken':
                        $this->apiSendUserCreationToken($apiResponse);
                        break;
                    case 'updatePassword':
                        $this->apiUpdatePassword($apiResponse);
                        break;
                    case 'listUsers':
                        $this->apiListUsers($apiResponse);
                        break;
                    case 'userPropEditor':
                        $this->apiUserPropEditor($apiResponse);
                        break;
                    case 'getInit':
                        $this->apiGetInit($apiResponse);
                        break;
                    case 'sendMessage':
                        $this->apiSendMessage($apiResponse);
                        break;
                    case 'joinChat':
                        $this->apiJoinChat($apiResponse);
                        break;
                    case 'getUpdates':
                        $this->apiGetUpdates($apiResponse);
                        break;
                    case 'chatSearch':
                        $this->apiChatSearch($apiResponse);
                        break;
                    case 'initPrivateChat':
                        $this->apiInitPrivateChat($apiResponse);
                        break;
                    case 'newGroupChat':
                        $this->apiNewGroupChat($apiResponse);
                        break;
                    case 'leaveChat':
                        $this->apiLeaveChat($apiResponse);
                        break;
                    default:
                        $apiResponse->setError('Unknown/unspecified command.', APIResponse::$i18n[LOCALE]['E_UNSPECIFIED_COMMAND'], 500);
                        break;
                }
                die($apiResponse);
            }
            else if($_SERVER['REQUEST_METHOD'] === 'GET')
            {
                // GET Request
                switch($_GET['q'])
                {
                    default:
                        $apiResponse->setError('Unknown/unspecified command.', APIResponse::$i18n[LOCALE]['E_UNSPECIFIED_COMMAND'], 500);
                        break;
                }
                die($apiResponse);
            }
            else
            {
                trigger_error('POST or GET permitted, none used.', E_USER_ERROR);
            }
        }
        
        /**
         * NOP, from 0-60sec
         * @param APIResponse $apiResponse
         * @param int $time
         */
        public function apiNOP($apiResponse, $time)
        {
            if(utility::could_be_int($time))
            {
                if($time>=0 && $time<=60)
                {
                    sleep($time);
                }
                else
                {
                    trigger_error('ILLEGAL ARGUMENT $time', E_USER_ERROR);
                }
            }
            else
            {
                trigger_error('ILLEGAL ARGUMENT $time', E_USER_ERROR);
            }
            $apiResponse->setSuccess();
        }
        
        /**
         * @param APIResponse $APIResponse
         * @return void
         */
        public function apiSendUserCreationToken($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'You need to be logged in to use this function.');
            else if(!$this->userRef->is_admin())
                return $APIResponse->setError('Unauthorized', 'You don\'t have the permission to perform this action.');
            $email = $_POST['email'];
            
            if($this->sendUserCreationToken($email))
                $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $email
         */
        public function sendUserCreationToken($email)
        {
            if(!utility::is_valid_email_address($email))
            {
                return false;
            }
            
            $regToken = $this->createNewRegToken();
            if($regToken === false)
                return false;
            
            ob_start();
            require INC_DIR.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'newUserCreationMail'.DIRECTORY_SEPARATOR.'html.php';
            $mail_html = ob_get_clean();
            $mail_css = file_get_contents(INC_DIR.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'newUserCreationMail'.DIRECTORY_SEPARATOR.'css.css');
            
            $cssToInlineStyles = new \TijsVerkoyen\CssToInlineStyles\CssToInlineStyles();
            
            $cssToInlineStyles->setHTML('<?xml encoding="UTF-8">'.$mail_html);
            $cssToInlineStyles->setCSS($mail_css);

            $inlinedCSSMail = $cssToInlineStyles->convert();
            
            $to = $email;

            $organization = 'Axel Latvala Apps';
            $appName = 'Reaktor Chat Service';
            $appHost = 'reaktor-chat-service.alatvala.fi';
            
            // subject
            $subject = $appName.' - New User';
            $subject = "=?UTF-8?B?".base64_encode($subject)."?=";

            // To send HTML mail, the Content-type header must be set
            $rn = "\n";
            $headers  = 'MIME-Version: 1.0' . $rn;
            $headers .= 'Content-type: text/html; charset=utf-8' . $rn;

            // Additional headers
            $headers .= 'From: =?UTF-8?B?'.base64_encode($organization).'?= <system@'.$appHost.'>' . $rn;
            $headers .= 'Content-transfer-encoding: base64' . $rn . $rn;

            $message = chunk_split(base64_encode($inlinedCSSMail));

            // Mail it

            mail($to, $subject, $message, $headers);
            
            return true;
        }
        
        /**
         * Handles registering users through the API
         * @param APIResponse $APIResponse
         */
        public function apiRegister($APIResponse)
        {
            $not_set = array();
            if(REGTOKEN_REQUIRED && !isset($_POST['regToken']))
            {
                $not_set[] = "Registration Token";
            }
            if(!isset($_POST['username']))
            {
                $not_set[] = "Username";
            }
            if(!isset($_POST['password']))
            {
                $not_set[] = "Password";
            }
            if(!isset($_POST['email']))
            {
                $not_set[] = "eMail";
            }
            if(!isset($_POST['firstname']))
            {
                $not_set[] = "First Name";
            }
            if(!isset($_POST['lastname']))
            {
                $not_set[] = "Last Name";
            }
            if(!isset($_POST['birthdate']))
            {
                $not_set[] = "Birthdate";
            } 
            if(!isset($_POST['gender']))
            {
                $not_set[] = "Gender";
            }
            if(count($not_set)>0)
            {
                $raw['success'] = false;
                $raw['errormsg'] = "Missing fields: ";
                $raw['error_fields'] = array();
                foreach($not_set as $value)
                {
                    $raw['error_fields'][] = $value;
                    $raw['errormsg'] .= $value . ", ";
                }
                return $APIResponse->setError(trim($raw['errormsg'], ", "), trim($raw['errormsg'], ", "));
            }
            else
            {
                $regToken = null;
                if(REGTOKEN_REQUIRED)
                    $regToken = $_POST['regToken'];
                
                if(CAPTCHA_REQUIRED && !$this->verifyRecaptcha($_POST['captcha']))
                    return $APIResponse->setError('Recaptcha failed.', 'The captcha you entered seems to be invalid.');
                
                $gender = null;
                if(!empty($_POST['gender']) && $_POST['gender'] !== '-')
                    $gender = (int)$_POST['gender'];
                    
                $raw = $this->register($_POST['username'], $_POST['password'], $_POST['email'], $_POST['firstname'],
                        $_POST['lastname'], $_POST['birthdate'], $gender, $regToken);
                if($raw['success'] === true)
                {
                    $APIResponse->setSuccess();
                    $APIResponse->setData(['user_uuid' => $raw['user_uuid']]);
                }
                else
                    return $APIResponse->setError($raw['errormsg'], $raw['errormsg']);
            }
        }
        
        /**
         * 
         * @param \APIResponse $APIResponse
         * @return boolean
         */
        public function apiLogin($APIResponse)
        {
            $not_set = array();
            if(!isset($_POST['user']))
            {
                $not_set[] = "User";
            }
            if(!isset($_POST['password']))
            {
                $not_set[] = "Password";
            }
            if(count($not_set)>0)
            {
                $sysErr = "Missing fields: ";
                foreach($not_set as $value)
                    $sysErr .= $value . ", ";
                return $APIResponse->setError(trim($sysErr, ", "), "The required fields are not set.");
            }
            $user = $this->login($_POST['user'], $_POST['password']);
            
            if($user->logged_in() !== false)
            {
                $APIResponse->setSuccess();
                $APIResponse->setData(array('user' => $user->toArray()));
            }
            else
                return $APIResponse->setError($user->error_message(), $user->error_message());
        }
        
        /**
         * 
         * @param \APIResponse $apiResponse
         */
        public function apiLogout($apiResponse)
        {
            $this->logout();
            $apiResponse->setSuccess();
        }
        
        /**
         * Logs out the user, destroying the session
         * @return nothing
         */
        public function logout()
        {
            return $this->userRef->log_out();
        }
        
        /**
         * Return the next regToken increment
         * @return int Increment value
         */
        public function getRegTokenIncrement()
        {
            $mysql_db = $this->getAppMySQLHandle();
            $query = "SELECT val FROM increments WHERE increments.type = 'reg_token'";
            $mysql_db->query($query);
            if(!$mysql_db->result)
            {
                trigger_error($mysql_db->getFormattedError(), E_USER_ERROR);
            }
            $tokenValRow = $mysql_db->result->fetch_assoc();
            return $tokenValRow['val'];
        }
        
        /**
         * 
         * @param type $tokenVal
         */
        public function incrementRegToken($tokenVal)
        {
            $mysql_db = $this->getAppMySQLHandle();
            
            $query = "UPDATE increments SET val = ".(int)++$tokenVal." WHERE increments.type = 'reg_token'";
            $mysql_db->query($query);
            if(!$mysql_db->result)
            {
                trigger_error('MySQL('.$mysql_db->error_code().'): '.$mysql_db->error_str(), E_USER_ERROR);
            }
        }
        
        /**
         * 
         * @param type $regToken
         */
        public function insertNewRegToken($regToken)
        {
            $mysql_db = $this->getAppMySQLHandle();
            
            $query = "INSERT INTO regToken (`token`) VALUES ('".$regToken."')";
            $mysql_db->query($query);
            if($mysql_db->affected_rows()!==1)
            {
                trigger_error('MySQL('.$mysql_db->error_code().'): '.$mysql_db->error_str(), E_USER_ERROR);
            }
        }
        
        /**
         * Creates a regtoken
         * @return string
         */
        public function createNewRegToken()
        {
            $curRegTokenVal = $this->getRegTokenIncrement();
            $this->incrementRegToken($curRegTokenVal);
            $regToken = base_convert($curRegTokenVal, 10, 36);
            $this->insertNewRegToken($regToken);
            return $regToken;
        }
        
        /**
         * Checks for active tokens
         * @param string $token
         * @return boolean
         */
        public function isActiveToken($token)
        {
            $mysql_db = $this->getAppMySQLHandle();
            
            $query = "SELECT * FROM regToken WHERE token = '".$mysql_db->real_escape_string($token)."'";
            $mysql_db->query($query);
            if(!$mysql_db->result)
            {
                trigger_error('MySQL ('.$mysql_db->error_code().'):'.$mysql_db->error_str(), E_USER_ERROR);
                return false;
            }
            if($mysql_db->result->num_rows < 1)
            {
                return false;
            }
            return true;
        }
        
        /**
         * Removes the regToken from tokens
         * @param string $token
         * @return boolean
         */
        public function deactivateToken($token)
        {
            $mysql_db = $this->getAppMySQLHandle();
            
            $query = "DELETE FROM regToken WHERE token = '".$mysql_db->real_escape_string($token)."'";
            if(!$mysql_db->query($query))
            {
                return false;
            }
            if($mysql_db->affected_rows() !== 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        /**
         * 
         * @param string $inputUsername
         * @param string $password
         * @param string $email
         * @param string $first_name
         * @param string $last_name
         * @param int $birthdate valid timestamp
         * @param int $gender 1/0
         * @param string $regToken
         * @return array consisting of atleast 'success'
         */
        public function register($inputUsername, $password, $email, $first_name, $last_name, $birthdate, $gender, $regToken = '')
        {
            $mysql_db = $this->getAppMySQLHandle();
            
            $errornous_field = array();
            $field_error = array();
            
            $new_user = new User(NULL, $mysql_db, $this->getLoginMySQLHandle());
            $username = trim($inputUsername);
            if(REGTOKEN_REQUIRED && empty($regToken))
            {
                $errornous_field[] = "RegToken";
                $field_error[] = "Registration token is absent";
            }
            elseif(REGTOKEN_REQUIRED && !$this->isActiveToken($regToken))
            {
                $errornous_field[] = "RegToken";
                $field_error[] = "Errornous registration token";
            }
            if(empty($username))
            {
                $errornous_field[] = "Username";
                $field_error[] = "The supplied username is empty";
            }
            elseif(strlen($username)<REAKTOR_CHAT_SERVICE_MIN_LEN_USERNAME)
            {
                $errornous_field[] = "Username";
                $field_error[] = "Username was too short (min ".REAKTOR_CHAT_SERVICE_MIN_LEN_USERNAME.' chars)';
            }
            elseif($this->userRef->user_exists_username($username) == array(true, true))
            {
                $errornous_field[] = "Username";
                $field_error[] = "Occupied username";
            }
            elseif(!$new_user->username($username))
            {
                $errornous_field[] = "Username";
                $field_error[] = $new_user->error_message();
            }
            if(empty($password))
            {
                $errornous_field[] = "Password";
                $field_error[] = "Password can't be empty";                            
            }
            elseif(strlen($password)<REAKTOR_CHAT_SERVICE_MIN_LEN_PASSWORD)
            {
                $errornous_field[] = "Password";
                $field_error[] = "Password is too short. (Min. ".REAKTOR_CHAT_SERVICE_MIN_LEN_PASSWORD." chars)";
            }
            if(empty($email))
            {
                $errornous_field[] = "Email";
                $field_error[] = "Email can't be empty";
            }
            elseif($this->userRef->user_exists_email($email) == array(true, true))
            {
                $errornous_field[] = 'Email';
                $field_error[] = 'Email is already registered';
            }
            elseif(!$new_user->email($email))
            {
                $errornous_field[] = 'Email';
                $field_error[] = $new_user->error_message();                            
            }
            if(empty($first_name))
            {
                $errornous_field[] = 'First Name';
                $field_error[] = 'First name was empty';
            }
            elseif(!$new_user->first_name($first_name))
            {
                $errornous_field[] = 'First Name';
                $field_error[] = $new_user->error_message();
            }
            if(empty($last_name))
            {
                $errornous_field[] = 'Last Name';
                $field_error[] = 'Last name was empty';
            }
            elseif(!$new_user->last_name($last_name))
            {
                $errornous_field[] = 'Last Name';
                $field_error[] = $new_user->error_message();
            }
            if(empty($birthdate))
            {
                $errornous_field[] = 'Birthdate';
                $field_error[] = 'Birthdate was empty';
            }
            elseif(!$new_user->birthdate($birthdate))
            {
                $errornous_field[] = 'Birthdate';
                $field_error[] = $new_user->error_message();
            }
            if(empty($gender))
            {
                // Not required
            }
            elseif($gender !== USER_PROPERTY_VAL_GENDER_FEMALE && $gender !== USER_PROPERTY_VAL_GENDER_MALE)
            {
                $errornous_field[] = "Gender";
                $field_error[] = "Errornous gender (valid genders are Male/Female)";
            }
            else
            {
                if(!$new_user->gender($gender))
                {
                    $errornous_field[] = "Gender";
                    $field_error[] = "Error setting gender.";
                }
            }
            if(count($errornous_field) > 0)
            {
                $raw['success'] = false;
                $raw['errormsg'] = 'Errornous data. '.PHP_EOL;
                foreach ($errornous_field as $index => $field)
                {
                    $raw['errormsg'] .= $field.": ".$field_error[$index].'.'.PHP_EOL;
                }
                $raw['errormsg'] = trim($raw['errormsg'], ' ,');
                $raw['errornous_field'] = $errornous_field;
                $raw['field_error'] = $field_error;
                return $raw;
            }
            else
            {
                if($new_user->save_data())
                {
                    if($new_user->set_password($password))
                    {
                        $this->deactivateToken($regToken);
                        $raw['success'] = true;
                        $raw['user_uuid'] = $new_user->user_uuid();
                        return $raw;
                    }
                    else
                    {
                        $raw['success'] = false;
                        $raw['errormsg'] = $new_user->error_message();
                        return $raw;
                    }
                }
                else
                {
                    $raw['success'] = false;
                    $raw['errormsg'] = $new_user->error_message();
                    return $raw;
                }
            }
        }
        
        /**
         * Gets the mysql handle for login db.
         * @return \mysql_DB
         */
        private function getLoginMySQLHandle()
        {
            // For simplicity, grant access to the app user to the login db.
            $mysql_db = new mysql_DB(LOGIN_DATABASE_HOST, LOGIN_DATABASE_USER, LOGIN_DATABASE_PASSWORD, LOGIN_DATABASE_NAME);
            if(!$mysql_db->is_connected())
            {
                trigger_error('MySQL('.$mysql_db->error_code().'): '.$mysql_db->error_str(), E_USER_ERROR);
            }
            return $mysql_db;
        }
        
        /**
         * Gets the mysql handle for login db.
         * @return \mysql_DB
         */
        private function getAppMySQLHandle()
        {
            if(get_class($this->masterHandle) === 'mysql_DB' && $this->masterHandle->is_connected())
            {
                return $this->masterHandle;
            }
            $mysql_db = new mysql_DB(APP_DATABASE_HOST, APP_DATABASE_USER, APP_DATABASE_PASSWORD, APP_DATABASE_NAME);
            if(!$mysql_db->is_connected())
            {
                trigger_error($mysql_db->getFormattedError(), E_USER_ERROR);
            }
            return $mysql_db;
        }
        
        /**
         * 
         * @param type $username
         * @param type $password
         * @return boolean|\User
         */
        public function login($username, $password)
        {
            $user = new User(NULL, $this->getAppMySQLHandle(), $this->getLoginMySQLHandle());
            if(!$user->do_login($username, $password))
            {
                return $user; // bool
            }
            return $user;
        }
        /**
         * 
         * @param \APIResponse $APIResponse
         */
        public function apiLoginStatus($APIResponse)
        {
            $APIResponse->setData(array
            (
                'loggedIn' => $this->loginStatus(), 
                'user' => $this->userRef->toArray()
            ));
            $APIResponse->setSuccess();
        }
        
        /**
         * true indicating logged in
         * @return boolean
         */
        public function loginStatus()
        {
            return $this->userRef->logged_in();
        }

        /**
         * 
         * @param string $response
         * @return boolean
         */
        public function verifyRecaptcha($response)
        {
            $url = RECAPTCHA_SITE_VERIFY_URL;
            $data = ['secret' => RECAPTCHA_SECRET_KEY, 'response' => $response];

            $options = [
                'http' => [
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                ]
            ];
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === false)
            {
                // Optional error handling
                return false;
            }
            $resObj = json_decode($result, true);
            if($resObj['success'] == true)
                return true;
            return false;
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiUpdatePassword($APIResponse)
        {
            $not_set = [];
            if(!isset($_POST['oldPass']))
                $not_set[] = 'oldPass';
            if(!isset($_POST['newPass']))
                $not_set[] = 'newPass';
            $num_not_set = count($not_set);
            if($num_not_set > 0)
            {
                $errorMsg = 'Missing fields: ';
                foreach($not_set as $key => $val)
                {
                    $errorMsg .= $val;
                    if($key !== ($num_not_set-1))
                        $errorMsg .= ", ";
                }
                return $APIResponse->setError($errorMsg, $errorMsg);
            }
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.');
            $res = $this->updatePassword($_POST['oldPass'], $_POST['newPass']);
            if($res[0] === true)
                $APIResponse->setSuccess();
            else
                return $APIResponse->setError($res[1], $res[1]);
        }
        
        /**
         * 
         * @param string $current
         * @param string $new
         * @return array[bool, string]
         */
        private function updatePassword($current, $new)
        {
            if(strlen(trim($new)) < REAKTOR_CHAT_SERVICE_MIN_LEN_PASSWORD)
                return [false, 'Too short a password'];
            if(!$this->userRef->logged_in())
                return [false, 'Not logged in'];
            $passTest = User::test_password($this->getLoginMySQLHandle(), $this->userRef->username(), $current);
            if(!$passTest)
                return [false, 'Password mismatch']; // Password mismatch
            if($this->userRef->set_password($new))
                return [true, 'Success'];
            return [false, $this->userRef->error_message()];
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiListUsers($APIResponse)
        {
            if(!$this->userRef->logged_in() || !$this->userRef->is_admin())
                return $APIResponse->setError('Unauthorized');
            $res = $this->listUsers();
            if($res[0] === false)
                return $APIResponse->setError($res[1]);
            $APIResponse->setSuccess();
            //$APIResponse->setData($res[1]);
            $APIResponse->setData(array_map(function($user){return $user->toArray();}, $res[1]));
        }
        
        /**
         * Lists ALL users
         * @return array with idx 0 indicating success, idx 1 data (errormessage on error and array of user objects on success)
         */
        private function listUsers()
        {
            $appDB = $this->getAppMySQLHandle();
            $loginDB = $this->getLoginMySQLHandle();
            $query = "SELECT CONV_BIN_TXT_UUID(user_uuid) AS user_uuid FROM authorized_users";
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $users = [];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            foreach($rows as $row)
            {
                $user = new User($row['user_uuid'], $this->getAppMySQLHandle(), $loginDB);
                if(!$user->load_failed())
                    $users[] = $user;
            }
            return [true, $users];
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiUserPropEditor($APIResponse)
        {
            if(!$this->userRef->logged_in() || !$this->userRef->is_admin())
                return $APIResponse->setError('Unauthorized');
            if(!isset($_POST['uuid']) || !utility::is_uuid($_POST['uuid']))
                return $APIResponse->setError('Invalid UUID');
            if(!isset($_POST['prop']) || empty($_POST['prop']))
                return $APIResponse->setError('Invalid prop');
            if(!isset($_POST['bool']))
                return $APIResponse->setError('Invalid bool');
            $res = $this->setUserProp($_POST['uuid'], $_POST['prop'], $_POST['bool']==="true"?true:false);
            if($res[0] !== true)
                return $APIResponse->setError($res[1]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $uuid
         * @param string $prop
         * @param boolean $bool
         * @return array idx 0 success, idx 1 data
         */
        private function setUserProp($uuid, $prop, $bool)
        {
            $user = new User($uuid, $this->getAppMySQLHandle(), $this->getLoginMySQLHandle());
            if($user->load_failed())
                return [false, $user->error_message()];
            switch($prop)
            {
                case 'is_admin':
                    $user->is_admin($bool);
                    break;
                case 'is_debuguser':
                    $user->is_debuguser($bool);
                    break;
                case 'is_banned':
                    $user->is_banned($bool);
                    break;
            }
            $saveRes = $user->save_data();
            if($saveRes !== true)
                return [false, $user->error_message()];
            return [true, 'Success'];
        }
        
        /**
         * api wrapper for getInit()
         * @param APIResponse $APIResponse
         */
        public function apiGetInit($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            $res = $this->getInit($this->userRef->user_uuid());
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            $APIResponse->setData(['chats' => $res[1], 'serverTime' => time()]);
            $APIResponse->setSuccess();
        }
        
        /**
         * Gets initial chats on login
         * @param string $user_uuid
         */
        private function getInit($user_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];

            $chatsRes = ChatSystem::getInit($this->getAppMySQLHandle(), $user_uuid);
            return $chatsRes;
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiSendMessage($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['text']))
                return $APIResponse->setError('Please supply message text', 'Please supply message text');
            if(!isset($_POST['chat_uuid']))
                return $APIResponse->setError('Please chat UUID', 'Please supply chat UUID');
            $res = $this->sendMessage($_POST['chat_uuid'], $_POST['text']);
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            $APIResponse->setData($res[1]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $chat_uuid
         * @param string $text
         * @return type
         */
        private function sendMessage($chat_uuid, $text)
        {
            $res = ChatSystem::newMessage($this->getAppMySQLHandle(), $this->userRef->user_uuid(), $chat_uuid, $text);
            if($res[0] === true)
                return [true, ['message_uuid' => $res[1]]];
            return $res;
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiJoinChat($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['chat_uuid']))
                return $APIResponse->setError('Please chat UUID', 'Please supply chat UUID');
            
            $joinRes = $this->joinChat($_POST['chat_uuid']);
            if($joinRes[0] !== true)
                return $APIResponse->setError($joinRes[1], $joinRes[1]);
            
            $chatInit = ChatSystem::getChatInit($this->getAppMySQLHandle(), $_POST['chat_uuid']);
            
            // $joinRes[1] will always be true, cause if it weren't we would have an error.
            $APIResponse->setData(['couldJoin' => $joinRes[1], 'chat' => $chatInit]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $chat_uuid
         * @return array[bool, bool|string] first bool to indicate wether we could carry out the operation successfully, second bool to indicate wether we were added to a chat or not, and on failure string with error message
         */
        private function joinChat($chat_uuid)
        {
            return $res = ChatSystem::attemptJoinChat($this->getAppMySQLHandle(), $chat_uuid, $this->userRef->user_uuid());
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiGetUpdates($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['since']))
                return $APIResponse->setError('Please provide since', 'Errornous request');
            if(!utility::is_valid_timestamp($_POST['since']))
                return $APIResponse->setError('since is not a valid timestamp.', 'Errornous request');
            
            $res = $this->getUpdates($this->userRef->user_uuid(), (int)$_POST['since']);
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            $APIResponse->setData($res[1]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $user_uuid
         * @param int $since valid timestamp
         * @return type
         */
        private function getUpdates($user_uuid, $since)
        {
            $chatsRes = ChatSystem::getUpdates($this->getAppMySQLHandle(), $user_uuid, $since);
            return $chatsRes;
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiChatSearch($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['searchText']))
                return $APIResponse->setError('Please provide searchText', 'Errornous request');
            if(strlen(utility::without_spaces($_POST['searchText'])) < 3)
                return $APIResponse->setError('Min. 3 nonwhitespace chars', 'Min. 3 nonwhitespace chars');
            $chatRes = $this->chatSearch($_POST['searchText']);
            if($chatRes[0] !== true)
                return $APIResponse->setError($chatRes[1], $chatRes[1]);
            
            $userRes = $this->userSearch($_POST['searchText']);
            if($userRes[0] !== true)
                return $APIResponse->setError($userRes[1], $userRes[1]);
            
            $APIResponse->setSuccess();
            $APIResponse->setData(['users' => $userRes[1], 'chats' => $chatRes[1]]);
        }
        
        /**
         * 
         * @param string $text
         * @return array idx 0 success, idx 1 error message or array of user data arrays
         */
        private function userSearch($text)
        {
            return ChatSystem::userSearch($this->getAppMySQLHandle(), $this->userRef->user_uuid(), $text);
        }
        
        /**
         * 
         * @param string $searchText
         * @return array idx 0 success, idx 1 error message or array of chat objects
         */
        private function chatSearch($searchText)
        {
            return ChatSystem::chatSearch($this->getAppMySQLHandle(), $this->userRef->user_uuid(), $searchText);
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiInitPrivateChat($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['withUser_uuid']))
                return $APIResponse->setError('Please supply other user\'s UUID', 'Errornous request');
            if(!utility::is_uuid($_POST['withUser_uuid']))
                return $APIResponse->setError('Invalid user UUID', 'Errornous request');
            
            $res = $this->initPrivateChat($_POST['withUser_uuid']);
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            
            $userInfoRes = ChatSystem::getEssentialUserDetails($this->getAppMySQLHandle(), $_POST['withUser_uuid']);
            $userInfoArr = $userInfoRes[1];
            
            $APIResponse->setData(['chat_uuid' => $res[1], 'withUser' => $userInfoArr]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $user_uuid
         * @return array[bool, string] boolean indicates wether it was successful, string is the chat uuid on success, and error message on failure
         */
        private function initPrivateChat($user_uuid)
        {
            return ChatSystem::newPrivateChat($this->getAppMySQLHandle(), $this->userRef->user_uuid(), $user_uuid);
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiNewGroupChat($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return [false, 'User not logged in.'];
            if(!isset($_POST['name']))
                return $APIResponse->setError('Please supply param name', 'Name not set');
            if(!isset($_POST['isPublic']))
                return $APIResponse->setError('Please supply param isPublic', 'isPublic not set');
            if(empty($_POST['name']) || strlen(trim($_POST['name'])) < 3)
                return $APIResponse->setError('length of name is minimum 3', 'The chatname is too short, minimum 3 characters.');
            
            $res = $this->newGroupChat($_POST['name'], $_POST['isPublic']==='true'?true:false);
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            
            $newChat = ChatSystem::getChatInit($this->getAppMySQLHandle(), $res[1]);
            if(count($newChat) < 1)
                return $APIResponse->setError('Error loading chat', 'Error loading chat');
            
            $APIResponse->setData(['chat' => $newChat]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $name
         * @param boolean $isPublic
         * @return array[bool, string] boolean indicates wether it was successful, string is the chat uuid on success, and error message on failure
         */
        private function newGroupChat($name, $isPublic)
        {
            if(!$this->userRef->logged_in())
                return [false, 'User not logged in.'];
            return ChatSystem::newGroupChat($this->getAppMySQLHandle(), $isPublic, $name, $this->userRef->user_uuid());
        }
        
        /**
         * 
         * @param APIResponse $APIResponse
         */
        public function apiLeaveChat($APIResponse)
        {
            if(!$this->userRef->logged_in())
                return $APIResponse->setError('User not logged in.', 'User not logged in.');
            if(!isset($_POST['chat_uuid']))
                return $APIResponse->setError('Not set chat_uuid', 'Please provide chat UUID');
            if(!utility::is_uuid($_POST['chat_uuid']))
                return $APIResponse->setError('Invalid UUID', 'Invalid UUID supplied.');
            $res = $this->leaveChat($_POST['chat_uuid']);
            if($res[0] !== true)
                return $APIResponse->setError($res[1], $res[1]);
            $APIResponse->setSuccess();
        }
        
        /**
         * 
         * @param string $chat_uuid
         * @return array[bool, string] Boolean indicating success, string being error message or chat uuid
         */
        public function leaveChat($chat_uuid)
        {
            if(!$this->userRef->logged_in())
                return [false, 'User not logged in.'];
            return ChatSystem::removeChatParticipation($this->getAppMySQLHandle(), $chat_uuid, $this->userRef->user_uuid());
        }
        
        
    }