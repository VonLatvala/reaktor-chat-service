<?php
    
    require_once 'inc/env_vars.php';
    require_once 'inc/classes/utility.class.php';
    /**
     * Created  13.11.2012 20:57 GMT+2<br />
     * Modified 27.07.2015 17:56 GMT+3, Axel Latvala
     * 
     * User is the class which manages the current user.
     *
     * @author  Axel Latvala
     * @version 1.4 eTunti Edition - MODIFIED FOR eOstaja - MODIFIED FOR eRuutu - Modified FOR TF-VICTORIA-ANSOKNING
     */
    define("USER_DETAILS_COL_USER_UUID", "user_uuid");
    define("USER_DETAILS_COL_EMAIL", "email");
    define("USER_DETAILS_COL_USERNAME", "username");
    define("USER_DETAILS_COL_FIRST_NAME", "first_name");
    define("USER_DETAILS_COL_LAST_NAME", "last_name");
    define("USER_DETAILS_COL_BIRTHDATE", "birthdate");
    define("USER_DETAILS_COL_REGISTERED", "registered");
    define("USER_DETAILS_COL_GENDER", "gender");
    
    define('APP_DB_TABLE_AUTH', 'authorized_users');
    
    define('APP_DB_TABLE_AUTH_COL_USER_UUID', 'user_uuid');
    define('APP_DB_TABLE_AUTH_COL_PERMIT_LOGIN', 'permit_login');
    define('APP_DB_TABLE_AUTH_COL_IS_ADMIN', 'is_admin');
    define('APP_DB_TABLE_AUTH_COL_IS_DEBUGUSER', 'is_debuguser');
    define('APP_DB_TABLE_AUTH_COL_IS_BANNED', 'is_banned');
    
    define("USER_DETAILS_SQL_SELECT_ALL", "CONV_BIN_TXT_UUID(".USER_DETAILS_COL_USER_UUID.") as ".USER_DETAILS_COL_USER_UUID.", ".USER_DETAILS_COL_EMAIL.", ".USER_DETAILS_COL_USERNAME.", ".USER_DETAILS_COL_FIRST_NAME.", ".USER_DETAILS_COL_LAST_NAME.", ".USER_DETAILS_COL_BIRTHDATE.", ".USER_DETAILS_COL_GENDER.", ".USER_DETAILS_COL_REGISTERED." ");

    define("USER_STRLEN_USERNAME", 32);
    define("USER_STRLEN_EMAIL", 100);
    define("USER_STRLEN_FIRST_NAME", 100);
    define("USER_STRLEN_LAST_NAME", 100);

    define("PASSWORD_DB_COL_USER_UUID", "user_uuid");
    define("PASSWORD_DB_COL_PASSWORD", "password");

    define("LOGIN_DB_TABLE_USER_DETAILS", "user_details");
    define("LOGIN_DB_TABLE_PASSWORD", "password");
    
    define("USER_PROPERTY_VAL_GENDER_FEMALE", 0);
    define("USER_PROPERTY_VAL_GENDER_MALE", 1);
    
    class User
    {
        // Data
        private $user_uuid;     // BINARY(16) to hold the user UUID
        private $email;         // VARCHAR(100) to hold the email address of the user
        private $username;      // VARCHAR(32) to hold the username of the user
        private $first_name;    // VARCHAR(100) to hold the first name of the user
        private $last_name;     // VARCHAR(100) to hold the last name of the user
        private $birthdate;     // INT(11) to hold the UNIX timestamp of the users' birth date
        private $registered;    // INT(11) to hold the registration date as UNIX timestamp
        private $gender;        // BINARY(1) to hold a "boolean" for gender. If unavailable, null.

        private $is_banned = false;
        private $is_debuguser = false;
        private $is_admin = false;
        
        // State
        private $load_failed;   // Used to determine if loading user data failed.
        private $is_new = true; // Used to determine if this object contains an existing or a new user.
        private $error_msg;
        private $logged_in = false;
        private $data_loaded = false;
        // References
        /**
         *
         * @var mysql_DB 
         */
        private $appDB;            // The database object (reference)
        /**
         *
         * @var mysql_DB 
         */
        private $loginDB;

        public function __construct($uuid, $appDB, $loginDB)
        {
            // Dependencies
            if(!class_exists("utility"))
            {
                trigger_error("Dependency requirements not met. Missing class: utility.", E_USER_ERROR);
                die();
            }
            if(!class_exists("mysql_DB"))
            {
                trigger_error("Dependency requirements not met. Missing class: mysql_DB.", E_USER_ERROR);
                die();
            }

            $this->appDB = $appDB;
            $this->loginDB = $loginDB;
            
            if(utility::is_uuid($uuid)) // Are we constructing with a valid UUID? If yes, assume existing user.
            {
                if($this->load_data($uuid))
                {
                    // Loading user data succeeded.
                    $this->logged_in = true;
                    $this->load_failed = false;
                    $this->is_new = false;
                }
                else
                {
                    // Loading user data failed.
                    $this->load_failed = true;
                    $this->is_new = false;
                }
            }
            else // We are not constructing with a valid UUID, assume new user.
            {
                $this->load_failed = false;
                $this->is_new = true;
            }

        }

        public function toArray()
        {
            return array
            (
                "logged_in" => $this->logged_in,
                "user_uuid" => $this->user_uuid,
                "email" => $this->email,
                "username" => $this->username,
                "first_name" => $this->first_name,
                "last_name" => $this->last_name,
                "birthdate" => $this->birthdate,
                "registered" => $this->registered,
                "is_admin" => $this->is_admin,
                "is_debuguser" => $this->is_debuguser,
                "is_banned" => $this->is_banned,
                "gender" => $this->gender,
                "gender_textual" => $this->textual_gender()
            );

        }

        public function do_login($user_name, $password)
        {
            $query = ""
                    . "SELECT".PHP_EOL
                    . "  CONV_BIN_TXT_UUID(".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_USER_UUID.")".PHP_EOL
                    . "  AS".PHP_EOL
                    . "  ".USER_DETAILS_COL_USER_UUID.",".PHP_EOL
                    . "  ".APP_DATABASE_NAME.'.'.APP_DB_TABLE_AUTH.'.'.APP_DB_TABLE_AUTH_COL_PERMIT_LOGIN.PHP_EOL
                    . "  AS".PHP_EOL
                    . "  ".APP_DB_TABLE_AUTH_COL_PERMIT_LOGIN.PHP_EOL
                    . "FROM".PHP_EOL
                    . "  ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.PHP_EOL
                    . "  LEFT OUTER JOIN ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_PASSWORD.PHP_EOL
                    . "    ON ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.APP_DB_TABLE_AUTH_COL_USER_UUID
                    .'     = '.LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_PASSWORD.'.'.PASSWORD_DB_COL_USER_UUID.PHP_EOL
                    . "  LEFT OUTER JOIN ".APP_DATABASE_NAME.'.'.APP_DB_TABLE_AUTH.PHP_EOL
                    . "    ON ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_USER_UUID
                    .'     = '.APP_DATABASE_NAME.'.'.APP_DB_TABLE_AUTH.'.'.APP_DB_TABLE_AUTH_COL_USER_UUID.PHP_EOL
                    . "WHERE".PHP_EOL
                    . "  (".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_EMAIL." = '".$user_name."'".PHP_EOL
                    . "  OR ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_USERNAME." = '".$user_name."')".PHP_EOL
                    . "  AND ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_PASSWORD.'.'.PASSWORD_DB_COL_PASSWORD.' = sha1(\''.$password.'\')';
            if(!$this->appDB->query($query))
            {
                $this->error_msg = $this->appDB->getFormattedError()."\n".$query;
                return false;   
            }
            if($this->appDB->result->num_rows<=0)
            {
                $this->error_msg = "Wrong user/email/password.";
                return false;
            }
            if(!($resarr = $this->appDB->result->fetch_assoc()))
            {
                $this->error_msg = $this->appDB->getFormattedError();
                return false;
            }
            if((int)$resarr[APP_DB_TABLE_AUTH_COL_PERMIT_LOGIN] !== 1 || is_null($resarr[APP_DB_TABLE_AUTH_COL_PERMIT_LOGIN]))
            {
                $this->error_msg = "Unauthorized user.";
                return false;
            }
            $user_uuid = $resarr[USER_DETAILS_COL_USER_UUID];
            $this->appDB->result->close();
            if(!$this->load_data($user_uuid))
            {
                $this->logged_in = false;
                return false;
            }
            if($this->is_banned())
            {
                $this->error_msg = "User BANNED!";
                $this->logged_in = false;
                return false;
            }
            $_SESSION['user_uuid'] = $user_uuid;
            $this->error_msg = null;
            $this->logged_in = true;
            return true;

        }

        public function login_without_password($user_name)
        {
            if (!$this->is_admin() || $this->is_debuguser())
            {
                $this->error_msg = "Current user does not have rights.";
                return false;
            }
            $query = "SELECT CONV_BIN_TXT_UUID(".USER_DETAILS_COL_USER_UUID.") as ".USER_DETAILS_COL_USER_UUID." FROM ".LOGIN_DB_TABLE_USER_DETAILS." WHERE ".USER_DETAILS_COL_EMAIL." = '".$user_name."' OR ".USER_DETAILS_COL_USERNAME." = '".$user_name."' OR ".USER_DETAILS_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$user_name."')";
            if($this->appDB->query($query))
            {
                if($this->appDB->result->num_rows<=0)
                {
                    $this->error_msg = "Invalid user/email/password."; # Password mismatch, but don't announce it.
                    $this->appDB->result->close();
                    return false;
                }
                $resarr = $this->appDB->result->fetch_assoc();
                if($resarr != false)
                {
                    if(!empty($resarr[USER_DETAILS_COL_USER_UUID]))
                    {
                        $user_uuid = $resarr[USER_DETAILS_COL_USER_UUID];
                    }
                    else
                    {
                        $this->error_msg = "Invalid user/email/password.";
                        $this->appDB->result->close();
                        return false;
                    }
                }
                else
                {
                    $this->error_msg = $this->appDB->getFormattedError();
                    $this->appDB->result->close();
                    return false;
                }
                $this->appDB->result->close();
            }
            else
            {
                $this->error_msg = $this->appDB->getFormattedError();
                $this->appDB->result->close();
                return false;
            }
            if($this->load_data($user_uuid))
            {
                $_SESSION['user_uuid'] = $user_uuid;
                $this->error_msg = null;
                $this->logged_in = true;
                return true;
            }
            else
            {
                $this->logged_in = false;
                return false;
            }

        }

        public function load_data($user_uuid)
        {
            $query = "".
                    "SELECT ".USER_DETAILS_SQL_SELECT_ALL." FROM ".LOGIN_DATABASE_NAME.".".LOGIN_DB_TABLE_USER_DETAILS." WHERE ".
                    USER_DETAILS_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$user_uuid."')";
            if($this->appDB->query($query))
            {
                $resarr = $this->appDB->result->fetch_assoc();
                $this->appDB->result->close();
                
                $this->data_loaded = true;
                $this->load_failed = false;

                $this->user_uuid = $user_uuid;
                $this->email = $resarr[USER_DETAILS_COL_EMAIL];
                $this->username = $resarr[USER_DETAILS_COL_USERNAME];
                $this->first_name = $resarr[USER_DETAILS_COL_FIRST_NAME];
                $this->last_name = $resarr[USER_DETAILS_COL_LAST_NAME];
                $this->birthdate = $resarr[USER_DETAILS_COL_BIRTHDATE];
                $this->registered = $resarr[USER_DETAILS_COL_REGISTERED];
                $gender = $resarr[USER_DETAILS_COL_GENDER];
                if(is_null($gender))
                    $this->gender = null;
                else
                    $this->gender = (int)$gender; // 1 or 0
                
                if($this->get_user_properties())
                    return true;
                else
                    return false;
            }
            else
            {
                $this->data_loaded = false;
                $this->load_failed = true;
                $this->error_msg = $this->appDB->getFormattedError();
                return false;
            }

        }
        
        private function get_user_properties()
        {
            $query = "SELECT * FROM authorized_users WHERE authorized_users.user_uuid = CONV_TXT_BIN_UUID('".$this->user_uuid."')";
            if($this->appDB->query($query))
            {
                $row = $this->appDB->result->fetch_assoc();
                $this->is_admin = $row['is_admin'] == "1";
                $this->is_debuguser = $row['is_debuguser'] == "1";
                $this->is_banned = $row['is_banned'] == "1";
                $this->appDB->result->close();
                return true;
            }
            $this->error_msg = $this->appDB->getFormattedError();
            return false;
        }

        public function logged_in()
        {
            return $this->logged_in;
        }

        public function log_out()
        {
            if(session_status() === PHP_SESSION_ACTIVE)
                session_destroy();
            $_SESSION = array();
        }

        public function error_message()
        {
            // Read only
            return $this->error_msg;
        }

        public function save_data()
        {
            if($this->is_new)
            {
                if(!utility::is_uuid($this->user_uuid))
                {
                    $this->user_uuid = utility::generate_uuid();
                }
                if(!is_numeric($this->birthdate))
                {
                    $this->birthdate = 0;
                }
                do
                {
                    $res = $this->user_exists_uuid($this->user_uuid);
                    if($res[0]==true)
                    {
                        if($res[1]==false)
                        {
                            // User does not exist.
                            $unique = true;
                        }
                        else
                        {
                            // User exists.
                            $unique = false;
                            $this->user_uuid = utility::generate_uuid();
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                while($unique==false);
                $safeGender = 'NULL';
                if($this->gender === USER_PROPERTY_VAL_GENDER_FEMALE)
                    $safeGender = '0';
                if($this->gender === USER_PROPERTY_VAL_GENDER_MALE)
                    $safeGender = '1';
                $query = "INSERT INTO ".
                        LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS
                        ." (".
                        USER_DETAILS_COL_USER_UUID.", ".
                        USER_DETAILS_COL_EMAIL.", ".
                        USER_DETAILS_COL_USERNAME.", ".
                        USER_DETAILS_COL_FIRST_NAME.", ".
                        USER_DETAILS_COL_LAST_NAME.", ".
                        USER_DETAILS_COL_BIRTHDATE.", ".
                        USER_DETAILS_COL_GENDER.", ".
                        USER_DETAILS_COL_REGISTERED
                        .") VALUES (".
                        "CONV_TXT_BIN_UUID('".$this->user_uuid."'), ".
                        "'".$this->loginDB->real_escape_string($this->email)."', ".
                        "'".$this->loginDB->real_escape_string($this->username)."', ".
                        "'".$this->loginDB->real_escape_string($this->first_name)."', ".
                        "'".$this->loginDB->real_escape_string($this->last_name)."', ".
                        "".$this->birthdate.", ".
                        "".$safeGender.", ".
                        "".time().
                        ")";
                if(!$this->loginDB->query($query))
                {
                    $this->error_msg = $this->loginDB->error_code().": ".$this->loginDB->error_str()."\n".$query;
                    return false;
                }
                $query = 'INSERT INTO '.
                        "\t".'`'.APP_DATABASE_NAME.'`.`authorized_users` '.
                        '('.
                        "\t".'user_uuid, '.
                        "\t".'permit_login '.
                        ') VALUES ('.
                        "\t"."CONV_TXT_BIN_UUID('".$this->user_uuid."'), ".
                        "\t".'b\'1\' '.
                        ')';
                if(!$this->appDB->query($query))
                {
                    $this->error_msg = $this->appDB->error_code().": ".$this->appDB->error_str()."\n".$query;
                    return false;
                }
                else
                {
                    $this->is_new = false;
                    return true;
                }
            }
            else
            {
                if(!utility::is_uuid($this->user_uuid))
                {
                    $this->error_msg = "Invalid user UUID.";
                    return false;
                }
                if(!is_numeric($this->birthdate))
                {
                    $this->error_msg = "Birthdate has to be numeric.";
                    return false;
                }
                $query = "UPDATE ".LOGIN_DATABASE_NAME.".".LOGIN_DB_TABLE_USER_DETAILS
                        ." SET ".
                        USER_DETAILS_COL_EMAIL."='".$this->loginDB->real_escape_string($this->email)."', ".
                        USER_DETAILS_COL_USERNAME."='".$this->loginDB->real_escape_string($this->username)."', ".
                        USER_DETAILS_COL_FIRST_NAME."='".$this->loginDB->real_escape_string($this->first_name)."', ".
                        USER_DETAILS_COL_LAST_NAME."='".$this->loginDB->real_escape_string($this->last_name)."', ".
                        USER_DETAILS_COL_BIRTHDATE."=".$this->birthdate.", ".
                        USER_DETAILS_COL_REGISTERED."=".$this->registered.
                        " WHERE ".
                        USER_DETAILS_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_uuid."')";
                if($this->loginDB->query($query))
                {
                    $propUpdateQry = "UPDATE ".APP_DATABASE_NAME.".".APP_DB_TABLE_AUTH." SET ".
                            APP_DB_TABLE_AUTH_COL_IS_ADMIN." = b'".($this->is_admin===true?1:0)."', ".
                            APP_DB_TABLE_AUTH_COL_IS_DEBUGUSER." = b'".($this->is_debuguser===true?1:0)."', ".
                            APP_DB_TABLE_AUTH_COL_IS_BANNED." = b'".($this->is_banned===true?1:0)."'".PHP_EOL.
                            "WHERE ".
                            APP_DB_TABLE_AUTH_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_uuid."')";
                    if($this->appDB->query($propUpdateQry))
                        return true;
                    else
                    {
                        $this->error_msg = $this->appDB->getFormattedError();
                        return false;
                    }
                }
                else
                {
                    $this->error_msg = $this->appDB->error_code().": ".$this->appDB->error_str()."\n".$query;
                    return false;
                }
            }

        }

        public function load_failed()
        {
            return $this->load_failed;

        }

        public function user_id()
        {
            return $this->user_id;

        }

        public function user_uuid()
        {
            return $this->user_uuid;

        }

        public function email($email = null)
        {
            if(!is_null($email))
            {
                if(utility::is_valid_email_address($email))
                {
                    $this->email = $email;
                    return true;
                }
                else
                {
                    $this->error_msg = "Invalid email.";
                    return false;
                }
            }
            return $this->email;

        }

        public function username($username = null)
        {
            if(!is_null($username))
            {
                $res = $this->user_exists_username($username);
                $allowed_username_regex = "/\b[\p{L}0-9]{".REAKTOR_CHAT_SERVICE_MIN_LEN_USERNAME.",".USER_STRLEN_USERNAME."}\b/";
                if(strlen($this->appDB->real_escape_string($username))>USER_STRLEN_USERNAME)
                {
                    $this->error_msg = "The supplied username is too long (max. ".USER_STRLEN_USERNAME." chars)";
                    return false;
                }
                elseif(!(preg_match($allowed_username_regex, $username)==1))
                {
                    $this->error_msg = "Username contains invalid characters.";
                    return false;
                }
                elseif($res[0] == true && $res[1] == true)
                {
                    $this->error_msg = "The provided username is taken.";
                    return false;             
                }
                elseif($res[0] == false)
                {
                    //$this->error_msg = "An unexpected error occurred. ".$this->appDB->error_str();
                    return false;
                }
                else
                {
                    $this->username = $username;
                    return true;
                }
            }
            return $this->username;

        }

        public function first_name($first_name = null)
        {
            if(!is_null($first_name))
            {
                $allowed_firstname_regex = "/\b\p{L}{2,".USER_STRLEN_FIRST_NAME."}\b/";
                if(strlen($this->appDB->real_escape_string($first_name))>USER_STRLEN_FIRST_NAME)
                {
                    $this->error_msg = "First name is too long (max. ".USER_STRLEN_FIRST_NAME." chars)";
                    return false;
                }
                else if(!preg_match($allowed_firstname_regex, utility::without_spaces($first_name)))
                {
                    $this->error_msg = "The supplied first name contains illegal characters.";
                    return false;
                }
                else
                {
                    $this->first_name = $first_name;
                    return true;
                }
            }
            return $this->first_name;

        }

        public function last_name($last_name = null)
        {
            if(!is_null($last_name))
            {
                $allowed_lastname_regex = "/\b\p{L}{2,".USER_STRLEN_LAST_NAME."}\b/";
                if(strlen($this->appDB->real_escape_string($last_name))>USER_STRLEN_LAST_NAME)
                {
                    $this->error_msg = "Last name is too long (max. ".USER_STRLEN_LAST_NAME.' chars)';
                    return false;
                }
                else if(!preg_match($allowed_lastname_regex, utility::without_spaces($last_name)))
                {
                    $this->error_msg = "The supplied last name contains illegal characters.";
                    return false;
                }
                else
                {
                    $this->last_name = $last_name;
                    return true;
                }
            }
            return $this->last_name;

        }

        public function birthdate($birthdate = null)
        {
            if(!is_null($birthdate))
            {
                if(utility::is_valid_timestamp($birthdate))
                {
                    $this->birthdate = $birthdate;
                    return true;
                }
                else
                {
                    $this->error_msg = "Invalid birthdate.";
                    return false;
                }
            }
            return $this->birthdate;

        }

        public function registered()
        {
            return $this->registered;

        }
        
        /**
         * Checks if the given UUID exists as a registered user
         * @todo What the hell is going on with these return arrays!? PATCH!
         * @param string $uuid
         * @return array
         */
        public function user_exists_uuid($uuid)
        {
            if(utility::is_uuid($uuid))
            {
                $query = "SELECT COUNT(*) as matches FROM ".LOGIN_DATABASE_NAME.".".LOGIN_DB_TABLE_USER_DETAILS." WHERE ".USER_DETAILS_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$uuid."')";
                if($this->loginDB->query($query))
                {
                    $resarr = $this->loginDB->result->fetch_assoc();
                    $this->loginDB->result->close();
                    if($resarr['matches']>0)
                    {
                        return array(true, true);
                    }
                    else
                    {
                        return array(true, false);
                    }
                }
                else
                {
                    $this->error_msg = $this->loginDB->getFormattedError();
                    return array(false, NULL);
                }
            }
            else
            {
                $this->error_msg = "Invalid UUID.";
                return array(false, $this->error_msg);
            }
        }
        
        /**
         * Checks if a user exists with the given username
         * @todo The return values are confusing. Plz fix.
         * @param string $inputUsername
         * @return array
         */
        public function user_exists_username($inputUsername)
        {
            $username = $this->loginDB->real_escape_string($inputUsername);
            $query = "SELECT COUNT(*) as matches FROM ".LOGIN_DB_TABLE_USER_DETAILS." WHERE ".USER_DETAILS_COL_USERNAME." = '".$username."'";
            if($this->loginDB->query($query))
            {
                $resarr = $this->loginDB->result->fetch_assoc();
                $this->loginDB->result->close();
                if($resarr['matches']>0)
                {
                    return array(true, true);
                }
                else
                {
                    return array(true, false);
                }
            }
            else
            {
                $this->error_msg = $this->loginDB->getFormattedError();
                return array(false, NULL);
            }
        }
        
        /**
         * Checks a user exists with the given email
         * @param type $email
         * @return type
         */
        public function user_exists_email($email)
        {
            $cleanMail = $this->appDB->real_escape_string($email);
            $query = "SELECT COUNT(*) as matches FROM ".LOGIN_DB_TABLE_USER_DETAILS." WHERE ".USER_DETAILS_COL_EMAIL." = '".$cleanMail."'";
            if($this->appDB->query($query))
            {
                $resarr = $this->appDB->result->fetch_assoc();
                $this->appDB->result->close();
                if($resarr['matches']>0)
                {
                    return array(true, true);
                }
                else
                {
                    return array(true, false);
                }
            }
            else
            {
                return array(false, NULL);
            }            
        }


        public function set_password($password)
        {
            if(!$this->is_new)
            {
                $query = "DELETE FROM `".LOGIN_DATABASE_NAME."`.`".LOGIN_DB_TABLE_PASSWORD."` WHERE `".PASSWORD_DB_COL_USER_UUID."` = CONV_TXT_BIN_UUID('".$this->user_uuid."') LIMIT 1";
                if(!$this->loginDB->query($query))
                {
                    $this->error_msg = $this->loginDB->getFormattedError();
                    return false;
                }
                $query = "INSERT INTO `password` (user_uuid, `password`) VALUES (CONV_TXT_BIN_UUID('".$this->user_uuid()."'), SHA1('".$this->appDB->real_escape_string($password)."'))";
                if($this->loginDB->query($query))
                {
                    return true;
                }
                else
                {
                    $this->error_msg = $this->loginDB->getFormattedError();
                    return false;
                }
            }
            else
            {
                $this->error_msg = "The user has to be saved first.";
                return false;
            }
        }

        public function match_password($password)
        {
            $query = "SELECT COUNT(*) FROM ".LOGIN_DB_TABLE_PASSWORD." WHERE ".PASSWORD_DB_COL_PASSWORD." = SHA1('".$password."') AND ".PASSWORD_DB_COL_USER_UUID." = CONV_TXT_BIN_UUID('".$this->user_uuid."')";
            if($this->appDB->query($query))
            {
                $resarr = $this->appDB->result->fetch_row();
                if($resarr != false)
                {
                    if($resarr[0] == 0 && $resarr[0] !== false) // maybe a === instead of ==?
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    $this->error_msg = $this->appDB->getFormattedError();
                    return -1;
                }
            }
            else
            {
                $this->error_msg = $this->appDB->getFormattedError();
                return -1;
            }
        }
        
        public function is_admin($set = null)
        {
            if(is_null($set))
                return $this->is_admin;
            $this->is_admin = (bool)$set;
        }
        
        public function is_debuguser($set = null)
        {
            if(is_null($set))
                return $this->is_debuguser;
            $this->is_debuguser = (bool)$set;
        }
        
        public function is_banned($set = null)
        {
            if(is_null($set))
                return $this->is_banned;
            $this->is_banned = (bool)$set;
        }
        
        public function gender($set = false)
        {
            if($set === false)
                return $this->gender;
            if($set === USER_PROPERTY_VAL_GENDER_FEMALE || $set === USER_PROPERTY_VAL_GENDER_MALE)
            {
                $this->gender = $set;
                return true;
            }
            if($set === null || empty($set))
            {
                $this->gender = null;
                return true;
            }
            return false;
        }
        
        public function textual_gender()
        {
            if($this->gender === null)
                return 'Unavailable';
            if($this->gender === USER_PROPERTY_VAL_GENDER_FEMALE)
                return 'Female';
            if($this->gender === USER_PROPERTY_VAL_GENDER_MALE)
                return 'Male';
            return 'Error getting gender.';
        }
        
        /**
         * 
         * @param mysql_DB $loginDB
         * @param string $username_or_email
         * @param string $password
         */
        public static function test_password($loginDB, $username_or_email, $password)
        {
            $clean_username_or_email = $loginDB->real_escape_string($username_or_email);
            $clean_password = $loginDB->real_escape_string($password);
            $query = ""
                . "SELECT".PHP_EOL
                . "  COUNT(*)".PHP_EOL
                . "  AS".PHP_EOL
                . "  num"."".PHP_EOL
                . "FROM".PHP_EOL
                . "  ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_PASSWORD.", ".PHP_EOL
                . "  ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.PHP_EOL
                . "WHERE".PHP_EOL
                . "  ".LOGIN_DATABASE_NAME.".".LOGIN_DB_TABLE_PASSWORD.".".PASSWORD_DB_COL_USER_UUID." = ".LOGIN_DATABASE_NAME.".".LOGIN_DB_TABLE_USER_DETAILS.".".USER_DETAILS_COL_USER_UUID.PHP_EOL
                . "  AND (".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_EMAIL." = '".$clean_username_or_email."'".PHP_EOL
                . "  OR ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_USER_DETAILS.'.'.USER_DETAILS_COL_USERNAME." = '".$clean_username_or_email."')".PHP_EOL
                . "  AND ".LOGIN_DATABASE_NAME.'.'.LOGIN_DB_TABLE_PASSWORD.'.'.PASSWORD_DB_COL_PASSWORD.' = sha1(\''.$clean_password.'\')';
            if(!$loginDB->query($query))
                trigger_error($loginDB->getFormattedError(), E_USER_ERROR);
            $row = $loginDB->result->fetch_assoc();
            $loginDB->result->close();
            if($row['num'] == "1")
                return true;
            return false;
        }
    }