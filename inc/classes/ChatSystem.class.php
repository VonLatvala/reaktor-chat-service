<?php

    /**
     * Handles the chats
     *
     * Created Jan 5, 2017 10:25:37 PM GMT+2
     * @author Axel Latvala
     */
    class ChatSystem
    {
        
        const NUM_MESSAGES_ON_INIT = CHAT_CONFIG_NUM_MESSAGES_ON_INIT;
        

        public function __construct()
        {
            
        }
        
        /**
         * Checks if two users have a private chat
         * @param mysql_DB $appDB
         * @param string $userOne
         * @param string $userTwo
         * @return array[bool, bool|string] boolean indicates wether it was successful, second bool indicates wether the chat does exists, and string error message on failure
         */
        public static function doUsersHavePrivateChat($appDB, $userOne, $userTwo)
        {
            if(!utility::is_uuid($userOne))
                return [false, "Invalid UUID for user one"];
            if(!utility::is_uuid($userTwo))
                return [false, "Invalid UUID for user two"];
            
            $query = "SELECT COUNT(*) as num".PHP_EOL
                    . "FROM".PHP_EOL
                    . "\tchats,".PHP_EOL
                    . "\tchat_participations".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "\tchats.chat_uuid = chat_participations.chat_uuid".PHP_EOL
                    . "\tAND chats.`type` = ".(int)Chat::TYPE_PRIVATE.PHP_EOL
                    . "\tAND chat_participations.user_uuid = CONV_TXT_BIN_UUID('".$userOne."')".PHP_EOL
                    . "\tAND chat_participations.chat_uuid IN (".PHP_EOL
                    . "\t\tSELECT chat_uuid FROM chat_participations".PHP_EOL
                    . "\t\tWHERE chat_participations.user_uuid = CONV_TXT_BIN_UUID('".$userTwo."'))".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            return [true, ((int)$row['num']) === 1];
        }
        
        /**
         * Creates a new private chat between two users
         * users should only have one private chat. "end to end" chat
         * @param mysql_DB $appDB app database handle
         * @param string $userOne textual `user_uuid` of one of the users participating
         * @param string $userTwo textual `user_uuid` of one of the users participating
         * @return array[bool, string] boolean indicates wether it was successful, string is the chat uuid on success, and error message on failure
         */
        public static function newPrivateChat($appDB, $userOne, $userTwo)
        {
            if(!utility::is_uuid($userOne))
                return [false, "Invalid UUID for user one"];
            if(!utility::is_uuid($userTwo))
                return [false, "Invalid UUID for user two"];
            
            $checkRes = ChatSystem::doUsersHavePrivateChat($appDB, $userOne, $userTwo);
            if($checkRes[0] !== true)
                return $checkRes;
            if($checkRes[1] === true)
                return [false, 'Users already have a private chat.'];
            
            try {
                $chat_uuid = utility::generate_uuid();
            }
            catch (Exception $e)
            {
                return [false, $e->getMessage()];
            }
            
            // Come to think of it, maybe this could be done with only one query. Well, they're ready now so can't be bothered to think about it.
            
            $chatCreationQuery = 'INSERT INTO'.PHP_EOL
                        .'chats'.PHP_EOL
                        .'(`chat_uuid`, `name`, `type`, `is_public`)'.PHP_EOL
                        .'VALUES'.PHP_EOL
                        .'(CONV_TXT_BIN_UUID(\''.$chat_uuid.'\'), \'\', '.(int)Chat::TYPE_PRIVATE.', b\'0\')';
            if(!$appDB->query($chatCreationQuery))
                return [false, $appDB->getFormattedError()];
            
            $curTimestamp = time();
            
            $userLinkingQuery = "INSERT INTO".PHP_EOL
                    .'chat_participations'.PHP_EOL
                    .'(`user_uuid`, `chat_uuid`, `is_admin`, `is_creator`, `is_banned`, `joined_timestamp`)'.PHP_EOL
                    .'VALUES'.PHP_EOL
                    .'(CONV_TXT_BIN_UUID(\''.$userOne.'\'), CONV_TXT_BIN_UUID(\''.$chat_uuid.'\'), b\'0\', b\'1\', b\'0\', '.$curTimestamp.'),'
                    .'(CONV_TXT_BIN_UUID(\''.$userTwo.'\'), CONV_TXT_BIN_UUID(\''.$chat_uuid.'\'), b\'0\', b\'0\', b\'0\', '.$curTimestamp.')';
            if(!$appDB->query($userLinkingQuery))
                return [false, $appDB->getFormattedError()];
            
            return [true, $chat_uuid];
        }
        
        /**
         * Creates a new group chat.
         * @param mysql_DB $appDB app database handle
         * @param boolean $isPublic wether to make the chat visible to others (people who know the textual uuid can join)
         * @param string $name name of the group chat
         * @param string $creatorUuid textual `user_uuid` of the creator
         * @return array[bool, string] boolean indicates wether it was successful, string is the chat uuid on success, and error message on failure
         */
        public static function newGroupChat($appDB, $isPublic, $name, $creatorUuid)
        {
            if(!utility::is_uuid($creatorUuid))
                return [false, "Invalid UUID for creator"];
            if(empty($name))
                return [false, "Chat name can't be empty!"];
            if(gettype($isPublic) !== 'boolean')
                return [false, "isPublic should be boolean."];
            
            try {
                $chat_uuid = utility::generate_uuid();
            }
            catch (Exception $e)
            {
                return [false, $e->getMessage()];
            }
            
            $chatCreationQuery = 'INSERT INTO'.PHP_EOL
                        .'chats'.PHP_EOL
                        .'(`chat_uuid`, `name`, `type`, `is_public`)'.PHP_EOL
                        .'VALUES'.PHP_EOL
                        .'(CONV_TXT_BIN_UUID(\''.$chat_uuid.'\'), \''.$appDB->real_escape_string($name).'\', '.(int)Chat::TYPE_GROUP.', b\''.($isPublic===true?'1':'0').'\')';
            if(!$appDB->query($chatCreationQuery))
                return [false, $appDB->getFormattedError()];
            
            $userLinkingQuery = "INSERT INTO".PHP_EOL
                    .'chat_participations'.PHP_EOL
                    .'(`user_uuid`, `chat_uuid`, `is_admin`, `is_creator`, `is_banned`, `joined_timestamp`)'.PHP_EOL
                    .'VALUES'.PHP_EOL
                    .'(CONV_TXT_BIN_UUID(\''.$creatorUuid.'\'), CONV_TXT_BIN_UUID(\''.$chat_uuid.'\'), b\'1\', b\'1\', b\'0\', '.time().')'.PHP_EOL;
            if(!$appDB->query($userLinkingQuery))
                return [false, $appDB->getFormattedError()];
            
            return [true, $chat_uuid];
        }
        
        /**
         * Lists all chats user with `user_uuid` $user_uuid is participating in.
         * @param mysql_DB $appDB app database handle
         * @param string $user_uuid textual `user_uuid` who is participating in these chats
         * @return array[bool, array[string]|string] boolean indicates wether it was successful, array consisting of chat uuids on success, and string error message on failure
         */
        public static function listMyAllChatUUIDs($appDB, $user_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid UUID"];
            
            $query = "SELECT CONV_BIN_TXT_UUID(chat_participations.chat_uuid) AS chat_uuid".PHP_EOL
                      ."FROM".PHP_EOL
                      ."\tchat_participations,".PHP_EOL
                      ."\tchats".PHP_EOL
                      ."WHERE".PHP_EOL
                      ."\tchat_participations.chat_uuid = chats.chat_uuid".PHP_EOL
                      ."\tAND chat_participations.user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')".PHP_EOL;
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            
            $chatUUIDs = [];
            while($row = $appDB->result->fetch_assoc())
                $chatUUIDs[] = $row['chat_uuid'];
            $appDB->result->close();
            
            return [true, $chatUUIDs];
        }
        
        /**
         * Lists all type X chats user with `user_uuid` $user_uuid is participating in.
         * @param mysql_DB $appDB app database handle
         * @param string $user_uuid textual `user_uuid` who is participating in these chats
         * @return array[bool, array[string]|string] boolean indicates wether it was successful, array consisting of chat uuids on success, and string error message on failure
         */
        public static function listMyTypeXChatUUIDs($appDB, $user_uuid, $chatType)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid UUID"];
            if(gettype($chatType) !== 'integer')
                return [false, 'Invalid chatType'];
            
            $query = "SELECT CONV_BIN_TXT_UUID(chat_participations.chat_uuid) AS chat_uuid".PHP_EOL
                      ."FROM".PHP_EOL
                      ."\tchat_participations,".PHP_EOL
                      ."\tchats".PHP_EOL
                      ."WHERE".PHP_EOL
                      ."\tchat_participations.chat_uuid = chats.chat_uuid".PHP_EOL
                      ."\tAND chats.`type` = ".(int)$chatType.PHP_EOL
                      ."\tAND chat_participations.user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')".PHP_EOL;
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            
            $chatUUIDs = [];
            while($row = $appDB->result->fetch_assoc())
                $chatUUIDs[] = $row['chat_uuid'];
            $appDB->result->close();
            
            return [true, $chatUUIDs];
        }
        
        /**
         * Lists all private chats user with `user_uuid` $user_uuid is participating in.
         * @param mysql_DB $appDB app database handle
         * @param string $user_uuid textual `user_uuid` who is participating in these chats
         * @return array[bool, array[string]|string] boolean indicates wether it was successful, array consisting of chat uuids on success, and string error message on failure
         */
        public static function listMyPrivateChatUUIDs($appDB, $user_uuid)
        {
            return ChatSystem::listMyTypeXChatUUIDs($appDB, $user_uuid, Chat::TYPE_PRIVATE);
        }
        
        /**
         * Lists all public group chats the user with `user_uuid` is participating in
         * @param mysql_DB $appDB the app database handle
         * @param string $user_uuid the `user_uuid` of the user we're getting public group chats of
         * @return array[bool, array[string]|string] boolean indicates wether it was successful, array consisting of chat uuids on success, and string error message on failure
         */
        public static function listMyPublicGroupChatUUIDs($appDB, $user_uuid)
        {
            return ChatSystem::listMyTypeXChatUUIDs($appDB, $user_uuid, Chat::TYPE_GROUP);
        }
        
        /**
         * Lists all public group chats `user_uuid` is not participating in
         * @param mysql_DB $appDB the app database handle
         * @param string $user_uuid the `user_uuid` of the user not participating in the chats
         * @return array[bool, array[string]|string] boolean indicates wether it was successful, array of `chat_uuid` on success, string with error message on failure
         */
        public static function listPublicNotMyGroupChatUUIDs($appDB, $user_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid UUID"];
            $query = "SELECT CONV_BIN_TXT_UUID(chat_uuid) AS chat_uuid".PHP_EOL
                      ."FROM chats".PHP_EOL
                      ."WHERE chat_uuid NOT IN (".PHP_EOL
                      ."\tSELECT CONV_BIN_TXT_UUID(chat_participations.chat_uuid) AS chat_uuid".PHP_EOL
                      ."\tFROM".PHP_EOL
                      ."\t\tchat_participations,".PHP_EOL
                      ."\t\tchats".PHP_EOL
                      ."\tWHERE".PHP_EOL
                      ."\t\tchat_participations.chat_uuid = chats.chat_uuid".PHP_EOL
                      ."\t\tAND chats.`type` = ".Chat::TYPE_GROUP.PHP_EOL
                      ."\t\tAND chat_participations.user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')".PHP_EOL
                      .")".PHP_EOL
                      ."AND chats.`type` = ".Chat::TYPE_GROUP.PHP_EOL
                      ."AND chats.is_public = b'1'";
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            
            $chatUUIDs = [];
            while($row = $appDB->result->fetch_assoc())
                $chatUUIDs[] = $row['chat_uuid'];
            $appDB->result->close();
            
            return [true, $chatUUIDs];
        }
        
        /**
         * Checks wether a chat exists
         * @param mysql_DB $appDB the app database handle
         * @param string $chat_uuid the `chat_uuid` of the chat we're checking
         * @return array[bool, bool|string] bool indicating success, second bool indicating if chat exists, or string on failure containing error message
         */
        public static function chatExists($appDB, $chat_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            
            $query = "SELECT COUNT(*) as num FROM chats WHERE chat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            if((int)$row['num'] !== 1)
                return [true, false];
            return [true, true];
        }
        
        /**
         * Loads chat metadata
         * @param mysql_DB $appDB the app database handle
         * @param string $chat_uuid `chat_uuid` of the chat to be loaded
         * @return array[bool, array|string] bool indicating success, array containing chat table fields, or string on failure containing error message
         */
        public static function loadChatMetadata($appDB, $chat_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid UUID"];
            
            $query = 'SELECT CONV_BIN_TXT_UUID(chat_uuid) AS chat_uuid, name, `type`, CAST(is_public AS unsigned) AS is_public'.PHP_EOL
                    .'FROM chats'.PHP_EOL
                    .'WHERE chat_uuid = CONV_TXT_BIN_UUID(\''.$chat_uuid.'\')'.PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            if($appDB->affected_rows() < 1)
                return [false, "No rows."];
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            return [true, $row];
        }
        
        /**
         * Constructs a Chat object from a database row of `chats` table
         * @param array $row `chats`.*
         * @return \Chat
         */
        public static function constructChatObjectFromDatabaseRow($row)
        {
            return new Chat($row['chat_uuid'], $row['name'], $row['type'], $row['is_public']=='1'?true:false);
        }
        
        /**
         * Constructs a ChatMessage object from a database row of `messages` table
         * @param array $row `messages`.*
         * @return \ChatMessage
         */
        public static function constructChatMessageObjectFromDatabaseRow($row)
        {
            return new ChatMessage($row['message_uuid'], $row['user_uuid'], $row['chat_uuid'], $row['timestamp'], $row['text'], $row['is_removed']=='1'?true:false);
        }
        
        /**
         * Batch mode function for ChatSystem::constructChatMessageObjectFromDatabaseRow()
         * @param type $rows
         * @return array[\ChatMessage]
         */
        public static function constructChatMessageObjectsFromDatabaseRows($rows)
        {
            return array_map(function($row){return ChatSystem::constructChatMessageObjectFromDatabaseRow($row);}, $rows);
        }
        
        /**
         * Gets a chat object with it's messages newer than $since
         * @param mysql_DB $appDB the app database handle
         * @param string $chat_uuid the `chat_uuid` of the chat we're getting the object of
         * @param int $since min timestamp of messages INCLUSIVE
         * @return array[bool, Chat|string] boolean indicates wether it was successful, Chat object on success, string with error message on failure
         */
        public static function getChatSince($appDB, $chat_uuid, $since)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid UUID"];
            if(!utility::is_valid_timestamp($since))
                return [false, "Invalid timestamp for since"];
            
            $chatMetadataRes = ChatSystem::loadChatMetadata($appDB, $chat_uuid);
            if($chatMetadataRes[0] !== true)
                return $chatMetadataRes;
            $chat = ChatSystem::constructChatObjectFromDatabaseRow($chatMetadataRes[1]);
            
            $query = "SELECT"
                    . "\tCONV_BIN_TXT_UUID(message_uuid) as message_uuid,".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(user_uuid) as user_uuid,".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(chat_uuid) as chat_uuid,".PHP_EOL
                    . "\ttimestamp,".PHP_EOL
                    . "\ttext,".PHP_EOL
                    . "\tCAST(is_removed AS unsigned) as is_removed".PHP_EOL
                    . "FROM".PHP_EOL
                    . "\tmessages".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "\tchat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL
                    . "\tAND timestamp > ".(int)$since.PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            
            $chat->messages = ChatSystem::constructChatMessageObjectsFromDatabaseRows($rows);
            return [true, $chat];
        }
        
        /**
         * Batch mode to get multiple chats for ChatSystem::getChatSince()
         * @param mysql_DB $appDB the app database handle
         * @param array[string] $chat_uuids array of the `chat_uuid`s of the chats we're getting the objects of
         * @param int $since min timestamp of messages
         * @return array[bool, array[Chat]|string] boolean indicates wether it was successful, array of Chat object on success, string with error message on failure
         */
        public static function getChatsSince($appDB, $chat_uuids, $since)
        {
            return [true, array_map(function($chat_uuid) use ($appDB, $since){
                return ChatSystem::getChatSince($appDB, $chat_uuid, $since);
            }, $chat_uuids)];
        }
        
        /**
         * Gets $limit newest messages from chat with `chat_uuid` $chat_uuid 
         * @param mysql_DB $appDB the app database handle
         * @param string $chat_uuid the `chat_uuid` of the chat we're getting the object of
         * @param type $limit number of newest messages we're getting
         * @return array[bool, Chat|string] boolean indicates wether it was successful, Chat object on success, string with error message on failure
         */
        public static function getChatLimit($appDB, $chat_uuid, $limit)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid UUID"];
            if(!utility::could_be_int($limit))
                return [false, "Invalid limit"];
            
            $chatMetadataRes = ChatSystem::loadChatMetadata($appDB, $chat_uuid);
            if($chatMetadataRes[0] !== true)
                return $chatMetadataRes;
            $chat = ChatSystem::constructChatObjectFromDatabaseRow($chatMetadataRes[1]);
            
            $query = "SELECT"
                    . "\tCONV_BIN_TXT_UUID(message_uuid) as message_uuid,".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(user_uuid) as user_uuid,".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(chat_uuid) as chat_uuid,".PHP_EOL
                    . "\ttimestamp,".PHP_EOL
                    . "\ttext,".PHP_EOL
                    . "\tCAST(is_removed AS unsigned) as is_removed".PHP_EOL
                    . "FROM".PHP_EOL
                    . "\tmessages".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "\tchat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL
                    . "ORDER BY timestamp DESC".PHP_EOL
                    . "LIMIT ".(int)$limit.PHP_EOL;
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            
            // Maybe let SQL do the reversing?
            $chat->messages = array_reverse(ChatSystem::constructChatMessageObjectsFromDatabaseRows($rows));
            
            return [true, $chat];
        }
        
        /**
         * Batch mode to get multiple chats for ChatSystem::getChatLimit()
         * @param mysql_DB $appDB the app database handle
         * @param array[string] $chat_uuids array of `chat_uuid`s of the chats we're getting the objects of
         * @param type $limit number of newest messages we're getting
         * @return array[bool, Chat|string] boolean indicates wether it was successful, Chat object on success, string with error message on failure
         */
        public static function getChatsLimit($appDB, $chat_uuids, $limit)
        {
            return [true, array_map(function($chat_uuid) use ($appDB, $limit){
                return ChatSystem::getChatLimit($appDB, $chat_uuid, $limit);
            }, $chat_uuids)];
        }
        
        /**
         * Wether a certain user participates in a certain chat
         * @param mysql_DB $appDB
         * @param string $user_uuid
         * @param string $chat_uuid
         * @return array[bool, bool|string] bool indicating success, second bool indicating wether or not the user does participate in the chat, and string on failure containing the error message.
         */
        public static function userParticipatesInChat($appDB, $user_uuid, $chat_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            
            $query = "SELECT".PHP_EOL
                    . "\tCOUNT(*) AS num".PHP_EOL
                    . "FROM".PHP_EOL
                    . "\tchat_participations".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "\tuser_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')".PHP_EOL
                    . "\tAND chat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL;
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            if((int)$row['num'] !== 1)
                return [true, false];
            return [true, true];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $user_uuid
         * @param string $chat_uuid
         * @return array idx 0 indicating success, idx 1 containing data or the errormessage
         */
        public static function getUserRelationToChat($appDB, $user_uuid, $chat_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            
            $query = "SELECT".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(user_uuid) AS user_uuid,".PHP_EOL
                    . "\tCONV_BIN_TXT_UUID(chat_uuid) AS chat_uuid,".PHP_EOL
                    . "\tCAST(is_admin AS unsigned) AS is_admin,".PHP_EOL
                    . "\tCAST(is_creator AS unsigned) AS is_creator,".PHP_EOL
                    . "\tCAST(is_banned AS unsigned) AS is_banned,".PHP_EOL
                    . "\tjoined_timestamp".PHP_EOL
                    . "FROM".PHP_EOL
                    . "\tchat_participations".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "\tuser_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')".PHP_EOL
                    . "\tAND chat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            if((int)$appDB->affected_rows() !== 1)
                return [false, "User is not participating in the chat"];
            
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            
            return [true, [
                    'joined_tomestamp' => $row['joined_timestamp'],
                    'is_admin' => $row['is_admin']=='1'?true:false,
                    'is_creator' => $row['is_creator']=='1'?true:false,
                    'is_banned' => $row['is_banned']=='1'?true:false
            ]];
        }
        
        /**
         * Submits a new message to a chat from a user
         * @param mysql_DB $appDB app database handle
         * @param string $fromUser textual `user_uuid` of the user from whom the message is
         * @param string $toChat textual `user_uuid` of the chat to which the message is
         * @param string $text the message to send
         * @return array[bool, string] boolean indicates wether it was successful, string is the message uuid on success, and error message on failure
         */
        public static function newMessage($appDB, $fromUser, $toChat, $text)
        {
            if(!utility::is_uuid($toChat))
                return [false, "Invalid chat UUID"];
            if(!utility::is_uuid($fromUser))
                return [false, "Invalid user UUID"];
            
            $userRelationToChatRes = ChatSystem::getUserRelationToChat($appDB, $fromUser, $toChat);
            if($userRelationToChatRes[0] !== true)
                return $userRelationToChatRes;
            $userRelationToChat = $userRelationToChatRes[1];
            
            if($userRelationToChat['is_banned'] === true)
                return [false, 'User is banned from chat. Cannot send message.'];
            
            try {
                $messageUUID = utility::generate_uuid();
            } catch (Exception $ex) {
                return [false, $ex->getMessage()];
            }
            
            $query = "INSERT INTO"
                    . "\tmessages".PHP_EOL
                    . "\t(`message_uuid`, `user_uuid`, `chat_uuid`, `timestamp`, `is_removed`, `text`)"
                    . "VALUES".PHP_EOL
                    . "\t(CONV_TXT_BIN_UUID('".$messageUUID."'), CONV_TXT_BIN_UUID('".$fromUser."'), CONV_TXT_BIN_UUID('".$toChat."'), ".time().", b'0', '".$appDB->real_escape_string($text)."');".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            return [true, $messageUUID];
        }
        
        /**
         * Attempts to join a group chat with `chat_uuid` $chat_uuid
         * This might fail if there exists no such chat (somebody is trying to join a nonexistent chat)
         * Won't work either with private chats since users are added to them on creation.
         * @param mysql_DB $appDB app database handle
         * @param string $chat_uuid `chat_uuid` of the chat we're attempting to join
         * @param string $user_uuid `user_uuid` of the user we're attempting to fuse to chat
         * @return array[bool, bool|string] first bool to indicate wether we could carry out the operation successfully, second bool to indicate wether we were added to a chat or not, and on failure string with error message
         */
        public static function attemptJoinChat($appDB, $chat_uuid, $user_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];
            
            /**
             * This check first because it's light and won't fail even if the chat doesn't exist.
             */
            $userParticipationRes = ChatSystem::userParticipatesInChat($appDB, $user_uuid, $chat_uuid);
            if($userParticipationRes[0] !== true)
                return $userParticipationRes;
            if($userParticipationRes[1] === true)
                return [false, "User already participates in that chat."];
            
            $chatExistanseRes = ChatSystem::chatExists($appDB, $chat_uuid);
            if($chatExistanseRes[0] !== true)
                return $chatExistanseRes;
            
            if($chatExistanseRes[1] !== true)
                return [false, "Chat does not exist"];
            
            $chatMetadataRes = ChatSystem::loadChatMetadata($appDB, $chat_uuid);
            if($chatMetadataRes[0] !== true)
                return $chatMetadataRes;
            
            $chat = ChatSystem::constructChatObjectFromDatabaseRow($chatMetadataRes[1]);
            
            if($chat->type === Chat::TYPE_PRIVATE)
                return [false, "Cannot join private chats"];
            
            $query = "INSERT INTO".PHP_EOL
                    . "\tchat_participations".PHP_EOL
                    . "(".PHP_EOL
                    . "\t`user_uuid`, `chat_uuid`, `is_admin`, `is_creator`, `is_banned`, `joined_timestamp`".PHP_EOL
                    . ")".PHP_EOL
                    . "VALUES".PHP_EOL
                    . "(".PHP_EOL
                    . "\tCONV_TXT_BIN_UUID('".$user_uuid."'), CONV_TXT_BIN_UUID('".$chat_uuid."'), b'0', b'0', b'0', ".time().PHP_EOL
                    . ")".PHP_EOL;
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            return [true, true];
        }
        
        /**
         * Gets all messages since $since from all chats participating in
         * @param mysql_DB $appDB app database handle
         * @param string $user_uuid textual `user_uuid` for whom we are getting updates
         * @param int $since timestamp for how old messages we are getting INCLUSIVE
         * @return array[bool, array[Chat]|string] boolean indicates wether it was successful, array consists of Chat objects on success, string is error message on failure
         */
        public static function getUpdates($appDB, $user_uuid, $since)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];
            if(!utility::is_valid_timestamp($since))
                return [false, "Invalid timestamp"];
            
            $chatUUIDsRes = ChatSystem::listMyAllChatUUIDs($appDB, $user_uuid);
            if($chatUUIDsRes[0] !== true)
                return $chatUUIDsRes;
            
            $chats = [];
            foreach($chatUUIDsRes[1] as $chatUUID)
            {
                $chats[$chatUUID] = [];
            }
            
            foreach($chats as $chatUUID => $val)
            {
                $chatRes = ChatSystem::getChatSince($appDB, $chatUUID, $since);
                if($chatRes[0] === true)
                {
                    if(count($chatRes[1]->messages) < 1)
                    {
                        unset($chats[$chatUUID]);
                    }
                    else
                    {
                        $messages = [];
                        foreach($chatRes[1]->messages as $messageObj)
                        {
                            $msgArr = [];
                            $essentialUserDetailsRes = ChatSystem::getEssentialUserDetails($appDB, $messageObj->user_uuid);
                            if($essentialUserDetailsRes[0] === true)
                                $msgArr['fromUser'] = $essentialUserDetailsRes[1];
                            else
                                $msgArr['fromUser'] = ['user_uuid' => $essentialUserDetailsRes[1].$messageObj->user_uuid, 'first_name' => null, 'last_name' => null, 'username' => null, 'email' => null];
                            $msgArr['messageUUID'] = $messageObj->message_uuid;
                            //$msgArr['chat_uuid'] = $messageObj->chat_uuid;
                            $msgArr['timestamp'] = $messageObj->timestamp;
                            $msgArr['text'] = $messageObj->text;
                            $messages[] = $msgArr;
                        }
                        $chatRes[1]->messages = $messages;
                        if(count($messages)>0)
                            $chatRes[1]->newestMessageTimestamp = $messages[count($messages)-1]['timestamp'];
                        else
                            $chatRes[1]->newestMessageTimestamp = 0;
                        $chatParticipatorRes = ChatSystem::getChatParticipants($appDB, $chatUUID);
                        if($chatParticipatorRes[0] === true)
                            $chatRes[1]->participants = $chatParticipatorRes[1];
                        $chats[$chatUUID] = $chatRes[1];
                    }
                }
            }
            
            $newChatUuidQuery = "SELECT CONV_BIN_TXT_UUID(chat_uuid) AS chat_uuid FROM chat_participations WHERE user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."') AND joined_timestamp >= ".$since;
            if(!$appDB->query($newChatUuidQuery))
                return [false, $appDB->getFormattedError()];
            $newChatUuidRows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            
            $newChats = [];
            
            $newChatUUIDs = array_map(function($row){return $row['chat_uuid'];}, $newChatUuidRows);
            foreach($newChatUUIDs as $chat_uuid)
                $newChats[$chat_uuid] = ChatSystem::getChatInit($appDB, $chat_uuid);
            
            $chatsRes = [true, ['updatedChats' => $chats, 'newChats' => $newChats]];
            
            return $chatsRes;
        }
        
        /**
         * Gets self::NUM_MESSAGES_ON_INIT newest messages per chat participating in
         * @param mysql_DB $appDB app database handle
         * @param string $user_uuid
         * @return array[bool, array[Chat]|string] boolean indicates wether it was successful, array consists of Chat objects on success, string is error message on failure
         */
        public static function getInit($appDB, $user_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID"];
            
            $chatUUIDsRes = ChatSystem::listMyAllChatUUIDs($appDB, $user_uuid);
            if($chatUUIDsRes[0] !== true)
                return $chatUUIDsRes;
            
            $chats = [];
            foreach($chatUUIDsRes[1] as $chatUUID)
            {
                $chats[$chatUUID] = [];
            }
            
            foreach($chats as $chatUUID => $val)
            {
                $chatInitRes = ChatSystem::getChatInit($appDB, $chatUUID);
                $chats[$chatUUID] = $chatInitRes;
            }
            
            $chatsRes = [true, $chats];
            
            return $chatsRes;
        }
        
        /**
         * Kind of the same as we have in the user management API, but only gets the UUIDs.
         * @param type $appDB
         * @param type $ownUUID
         * @return type
         */
        public static function listUserUUIDs($appDB, $ownUUID)
        {
            if(!utility::is_uuid($ownUUID))
                return [false, "Invalid user UUID"];
            
            $query = "SELECT CONV_BIN_TXT_UUID(user_uuid) as user_uuid FROM authorized_users WHERE user_uuid != CONV_TXT_BIN_UUID('".$ownUUID."')";
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            
            $uuids = array_map(function($row){return $row['user_uuid'];}, $appDB->result->fetch_all());
            return [true, $uuids];
        }
        
        public static function getEssentialUserDetails($appDB, $user_uuid)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID: $user_uuid"];
            
            $query = "SELECT CONV_BIN_TXT_UUID(user_uuid) as user_uuid, first_name, last_name, username, email FROM reaktor_chat_service_login.user_details WHERE".PHP_EOL
                    . "user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."')";
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $row = $appDB->result->fetch_assoc();
            $appDB->result->close();
            return [true, $row];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $chat_uuid
         * @return [[bool, array[string]|string]
         */
        public static function getChatParticipantUUIDs($appDB, $chat_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            $query = "SELECT CONV_BIN_TXT_UUID(user_uuid) as user_uuid FROM chat_participations WHERE chat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            return [true, array_map(function($row){return $row['user_uuid'];}, $rows)];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $chat_uuid
         * @return array[bool, array[array[user_uuid, first_name, last_name, username, email]]|string] Description
         */
        public static function getChatParticipants($appDB, $chat_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            
            $uuidRes = ChatSystem::getChatParticipantUUIDs($appDB, $chat_uuid);
            if($uuidRes[0] !== true)
                return $uuidRes;
            
            $users = [];
            foreach($uuidRes[1] as $user_uuid)
            {
                $userDataRes = ChatSystem::getEssentialUserDetails($appDB, $user_uuid);
                if($userDataRes[0] === true)
                    $users[] = $userDataRes[1];
            }
            
            return [true, $users];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $chat_uuid
         * @return array[bool, string] Boolean indicating success, string being error message or the chat object or null on not found
         */
        public static function getGroupChatByUUID($appDB, $chat_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID"];
            
            $chatExistsRes = ChatSystem::chatExists($appDB, $chat_uuid);
            if($chatExistsRes[0] === false)
                return $chatExistsRes;
            if($chatExistsRes[1] === true)
            {
                $chatMetadataRes = ChatSystem::loadChatMetadata($appDB, $chat_uuid);
                $chat = ChatSystem::constructChatObjectFromDatabaseRow($chatMetadataRes[1]);
                if((int)$chat->type === Chat::TYPE_GROUP)
                    return [true, $chat];
                else
                    return [true, null];
            }
            else
                return [true, null];
            return $chatExistsRes;
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $user_uuid
         * @param string $searchText
         * @return array idx 0 success, idx 1 error message or array of user data arrays
         */
        public static function userSearch($appDB, $user_uuid, $searchText)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, 'Invalid user UUID'];
            $cleanSearchText = $appDB->real_escape_string($searchText);
            $cleanSearchTextWithWildcards = '%'.str_replace(' ', '%', $cleanSearchText).'%';
            $query = "SELECT CONV_BIN_TXT_UUID(user_uuid) AS user_uuid".PHP_EOL
                    . "FROM reaktor_chat_service_login.user_details".PHP_EOL
                    . "WHERE".PHP_EOL
                    . "user_details.user_uuid != CONV_TXT_BIN_UUID('".$user_uuid."')"
                    . "\tAND (user_details.first_name LIKE '".$cleanSearchTextWithWildcards."'".PHP_EOL
                    . "\tOR user_details.last_name LIKE '".$cleanSearchTextWithWildcards."'".PHP_EOL
                    . "\tOR user_details.username LIKE '".$cleanSearchTextWithWildcards."'".PHP_EOL
                    . "\tOR user_details.email LIKE '".$cleanSearchTextWithWildcards."'".PHP_EOL
                    . "\tOR CONCAT_WS(' ', user_details.first_name, user_details.last_name) LIKE '".$cleanSearchTextWithWildcards."')".PHP_EOL;
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            
            if(count($rows) < 1)
                return [true, []];
            
            return [true, array_map(function($row) use ($appDB) {return ChatSystem::getEssentialUserDetails($appDB, $row['user_uuid'])[1];}, $rows)];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $user_uuid
         * @param string $searchText
         * @return array idx 0 success, idx 1 error message or array of chat objects
         */
        public static function chatSearch($appDB, $user_uuid, $searchText)
        {
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID: $user_uuid"];
            
            if(utility::is_uuid($searchText))
            {
                $groupChatRes = ChatSystem::getGroupChatByUUID($appDB, $searchText);
                if($groupChatRes[0] === false)
                    return $groupChatRes;
                if($groupChatRes[1] === null)
                    return [true, []];
                else
                    return [true, [$groupChatRes[1]]];
            }
            
            $cleanSearchText = $appDB->real_escape_string($searchText);
            $cleanSearchTextWithWildcards = '%'.str_replace(' ', '%', $cleanSearchText).'%';
            $query = "SELECT CONV_BIN_TXT_UUID(chat_uuid) AS chat_uuid FROM chats WHERE type = ".(int)Chat::TYPE_GROUP." AND is_public = b'1' AND name LIKE '".$cleanSearchTextWithWildcards."'";
            
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            $rows = $appDB->result->fetch_all(MYSQLI_ASSOC);
            $appDB->result->close();
            
            $groupchatUuidArr = array_map(function($row){return $row['chat_uuid'];}, $rows);
            
            // sloppy, but the loading of metadata should never fail since we JUST got the UUIDs from the database.
            $groupchatMetadataArr = array_map(function($chat_uuid) use ($appDB){return ChatSystem::constructChatObjectFromDatabaseRow(ChatSystem::loadChatMetadata($appDB, $chat_uuid)[1]);}, $groupchatUuidArr);
            
            return [true, $groupchatMetadataArr];
        }
        
        /**
         * 
         * @param mysql_DB $appDB
         * @param string $chat_uuid
         * @return empty array on error, array with init data on success (like all chats with about 50 newest messages)
         */
        public static function getChatInit($appDB, $chat_uuid)
        {
            $chatRes = ChatSystem::getChatLimit($appDB, $chat_uuid, ChatSystem::NUM_MESSAGES_ON_INIT);
            if($chatRes[0] === true)
            {
                $messages = [];
                foreach($chatRes[1]->messages as $messageObj)
                {
                    $msgArr = [];
                    $essentialUserDetailsRes = ChatSystem::getEssentialUserDetails($appDB, $messageObj->user_uuid);
                    if($essentialUserDetailsRes[0] === true)
                        $msgArr['fromUser'] = $essentialUserDetailsRes[1];
                    else
                        $msgArr['fromUser'] = ['user_uuid' => $essentialUserDetailsRes[1], 'first_name' => null, 'last_name' => null, 'username' => null, 'email' => null];
                    $msgArr['messageUUID'] = $messageObj->message_uuid;
                    $msgArr['timestamp'] = $messageObj->timestamp;
                    $msgArr['text'] = $messageObj->text;
                    $messages[] = $msgArr;
                }
                $chatRes[1]->messages = $messages;
                if(count($messages)>0)
                    $chatRes[1]->newestMessageTimestamp = $messages[count($messages)-1]['timestamp'];
                else
                    $chatRes[1]->newestMessageTimestamp = 0;
                $chatParticipatorRes = ChatSystem::getChatParticipants($appDB, $chat_uuid);
                if($chatParticipatorRes[0] === true)
                    $chatRes[1]->participants = $chatParticipatorRes[1];
                return $chatRes[1];
            }
            return [];
        }
        
        /**
         * Remove user from chat
         * @param mysql_DB $appDB
         * @param string $chat_uuid
         * @param string $user_uuid
         * @return array[bool, string] Boolean indicating success, string being error message or chat uuid
         */
        public static function removeChatParticipation($appDB, $chat_uuid, $user_uuid)
        {
            if(!utility::is_uuid($chat_uuid))
                return [false, "Invalid chat UUID: $chat_uuid"];
            if(!utility::is_uuid($user_uuid))
                return [false, "Invalid user UUID: $user_uuid"];
            
            $query = "DELETE FROM chat_participations WHERE user_uuid = CONV_TXT_BIN_UUID('".$user_uuid."') AND chat_uuid = CONV_TXT_BIN_UUID('".$chat_uuid."')";
            if(!$appDB->query($query))
                return [false, $appDB->getFormattedError()];
            return [true, $chat_uuid];
        }

    }